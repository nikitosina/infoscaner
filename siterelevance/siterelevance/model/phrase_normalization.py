from abc import ABC, abstractmethod


class PhraseNormalizationI(ABC):
    @abstractmethod
    def process(self, phrase: str) -> str:
        """abstract"""
