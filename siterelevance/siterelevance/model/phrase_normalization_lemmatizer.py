from abc import ABC, abstractmethod
from typing import Any, Callable, Collection, Dict, Optional

import pymorphy2
from nltk import WordNetLemmatizer

from siterelevance.model.language import EnglishLanguage, LanguageDeterminer, RussianLanguage, WordLanguage
from siterelevance.model.phrase_normalization import PhraseNormalizationI


class LemmatizerWrapper(ABC):
    _lemmatizer: Optional[Any] = None

    @abstractmethod
    def lemmatize(self, word: str) -> str:
        """abstract"""


class LemmatiserRus(LemmatizerWrapper):
    @classmethod
    def lemmatize(cls, word: str) -> str:
        if not cls._lemmatizer:
            cls._lemmatizer = pymorphy2.MorphAnalyzer()
        return cls._lemmatizer.parse(word)[0].normal_form


class LemmatiserEng(LemmatizerWrapper):
    @classmethod
    def lemmatize(cls, word: str) -> str:
        if cls._lemmatizer is None:
            cls._lemmatizer = WordNetLemmatizer()
        return cls._lemmatizer.lemmatize(word, pos='n')


class PhraseNormalizationLemmatizer(PhraseNormalizationI):
    def __init__(self, languages: Collection[WordLanguage], fallback_language: WordLanguage):
        self._languages = languages
        self._default_language = fallback_language
        self._lemmatizers: Dict[str, Callable[[str], str]] = {}

    def _get_lemmatizer(self, language: WordLanguage) -> Callable[[str], str]:
        language_name = language.get_name()

        if language_name not in self._lemmatizers:

            if language_name == RussianLanguage.get_name():
                lemmatizer = LemmatiserRus.lemmatize

            elif language_name == EnglishLanguage.get_name():
                lemmatizer = LemmatiserEng.lemmatize

            else:
                raise ValueError(f'Unknown language: {language_name}, cannot find a lemmatizer')

            self._lemmatizers[language_name] = lemmatizer

        return self._lemmatizers[language_name]

    def process(self, phrase: str) -> str:

        result = []

        for word in phrase.split():
            language = LanguageDeterminer.get_language_with_fallback(phrase, self._languages, self._default_language)
            lemmatizer = self._get_lemmatizer(language)
            result.append(lemmatizer(word))

        return ' '.join(result)
