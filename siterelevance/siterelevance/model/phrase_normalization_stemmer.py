from typing import Collection, Dict

from nltk import SnowballStemmer, StemmerI

from siterelevance.model.language import LanguageDeterminer, WordLanguage
from siterelevance.model.phrase_normalization import PhraseNormalizationI


class PhraseNormalizationStemmer(PhraseNormalizationI):
    def __init__(self, languages: Collection[WordLanguage], fallback_language: WordLanguage):
        self._languages = languages
        self._default_language = fallback_language
        self._stemmers: Dict[str, SnowballStemmer] = {}

    def _get_stemmer(self, language: WordLanguage) -> StemmerI:
        language_name = language.get_name()

        if language_name not in self._stemmers:
            self._stemmers[language_name] = SnowballStemmer(language_name)
        return self._stemmers[language_name]

    def process(self, phrase: str) -> str:

        result = []

        for word in phrase.split():
            language = LanguageDeterminer.get_language_with_fallback(phrase, self._languages, self._default_language)
            stemmer = self._get_stemmer(language)
            result.append(stemmer.stem(word))

        return ' '.join(result)
