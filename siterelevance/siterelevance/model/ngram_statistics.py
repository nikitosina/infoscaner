from collections import defaultdict
from typing import Dict, Optional


class NGramStatistics:
    def __init__(self, total: int):
        self._count = 0
        self._frequency: Optional[float] = None
        self._total = total

    def increment(self) -> None:
        self._count += 1

    @property
    def count(self) -> int:
        return self._count

    @property
    def frequency(self) -> float:
        if self._frequency is None:
            self._frequency = self._count / self._total
        return self._frequency

    @property
    def total(self) -> int:
        return self._total


class NGrams:
    def __init__(self, n: int):
        if n < 1:
            raise ValueError(f'ngram length must be greater than or equal to 1 (provided length is {n})')
        self._n = n

    def get_statistics(self, text: str) -> Dict[str, NGramStatistics]:
        tokens = text.split()

        total_count = max(0, len(tokens) - self._n + 1)

        ngrams_count: Dict[str, NGramStatistics] = defaultdict(lambda: NGramStatistics(total_count))

        for i in range(total_count):
            ngram = ' '.join(tokens[i:i+self._n])
            ngrams_count[ngram].increment()

        return ngrams_count
