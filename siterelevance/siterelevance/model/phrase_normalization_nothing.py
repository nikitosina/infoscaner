from siterelevance.model.phrase_normalization import PhraseNormalizationI


class PhraseNormalizationNothing(PhraseNormalizationI):
    def process(self, phrase: str) -> str:
        return phrase
