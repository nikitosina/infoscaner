from string import punctuation
from typing import Collection

import nltk
from nltk.corpus import stopwords

from siterelevance.model.language import LanguageDeterminer, WordLanguage


class StopWordsRemoval:
    def __init__(self, languages: Collection[WordLanguage], fallback_language: WordLanguage):
        self._languages = languages
        self._default_language = fallback_language
        self._stopwords = set(word for language in self._languages for word in stopwords.words(language.get_name()))

    @staticmethod
    def _remove_punctuation(tokens: Collection[str]) -> Collection[str]:
        return tuple(t for t in tokens if len(t) > 1 or t not in punctuation)

    def _remove_stop_words(self, tokens: Collection[str]) -> Collection[str]:
        return tuple(t for t in tokens if t not in self._stopwords)

    def _get_tokens(self, phrase: str) -> Collection[str]:
        language = LanguageDeterminer.get_language_with_fallback(phrase, self._languages, self._default_language)

        tokens = nltk.word_tokenize(phrase, language=language.get_name())
        return tokens

    def remove(self, phrase: str) -> str:
        text = phrase.lower()

        tokens = self._get_tokens(text)
        tokens = self._remove_stop_words(tokens)
        tokens = self._remove_punctuation(tokens)
        return ' '.join(tokens)
