import re
from typing import Iterable, Optional


class WordLanguage:
    _regexp = re.compile(r'')
    _name = ''

    @classmethod
    def get_name(cls) -> str:
        return cls._name

    @classmethod
    def contains_word(cls, word: str) -> bool:
        return bool(cls._regexp.match(word))


class RussianLanguage(WordLanguage):
    _regexp = re.compile(r'^[а-яА-Я0-9-\s\W]+$')
    _name = 'russian'


class EnglishLanguage(WordLanguage):
    _regexp = re.compile(r'^[a-zA-Z0-9-\s\W]+$')
    _name = 'english'


class LanguageDeterminer:
    @staticmethod
    def get_language(text: str, candidates: Iterable[WordLanguage]) -> Optional[WordLanguage]:
        result = None

        for language in candidates:
            if language.contains_word(text):
                result = language
                break

        return result

    @staticmethod
    def get_language_with_fallback(
            text: str,
            candidates: Iterable[WordLanguage],
            fallback: WordLanguage
    ) -> WordLanguage:
        language = LanguageDeterminer.get_language(text, candidates)

        if language:
            return language

        return fallback
