import os

import pytest

from siterelevance.model.language import EnglishLanguage, RussianLanguage
from siterelevance.model.phrase_normalization_lemmatizer import PhraseNormalizationLemmatizer

NLTK_DATA = os.getenv('NLTK_DATA')


@pytest.fixture
def languages():
    return (
        RussianLanguage(),
        EnglishLanguage(),
    )


@pytest.fixture
def default_language():
    return RussianLanguage()


@pytest.mark.skipif(not NLTK_DATA, reason='no path to nltk data directory is specified')
def test_lemmatizer_russian(languages, default_language):
    lemmatizer = PhraseNormalizationLemmatizer(languages, default_language)

    phrase = 'съешь ещё этих мягких французских булок да выпей чаю'

    expected = 'съесть ещё этот мягкий французский булка да выпить чай'

    result = lemmatizer.process(phrase)

    assert result == expected


@pytest.mark.skipif(not NLTK_DATA, reason='no path to nltk data directory is specified')
def test_lemmatizer_english(languages, default_language):
    for x in os.walk(NLTK_DATA):
        print(x[0])

    lemmatizer = PhraseNormalizationLemmatizer(languages, default_language)

    phrase = 'i am telling you funny stories'
    expected = 'i am telling you funny story'

    result = lemmatizer.process(phrase)

    assert result == expected
