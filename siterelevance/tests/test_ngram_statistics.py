import pytest

from siterelevance.model.ngram_statistics import NGrams


def test_ngram_statistics():
    n3_gram = NGrams(2)
    total = 5

    text = 'мама мыла раму мама мыла окно'

    expected = {
        'мама мыла': 2,
        'мыла раму': 1,
        'раму мама': 1,
        'мыла окно': 1,
    }

    result = n3_gram.get_statistics(text)

    assert sorted(result.keys()) == sorted(expected.keys())

    for key in result.keys():
        assert result[key].frequency == pytest.approx(expected[key] / total, abs=1e-6)
        assert result[key].total == total
        assert result[key].count == expected[key]


def test_ngram_non_positive():
    with pytest.raises(ValueError):
        NGrams(0)
