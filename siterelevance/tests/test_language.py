import pytest

from siterelevance.model.language import EnglishLanguage, LanguageDeterminer, RussianLanguage


@pytest.fixture
def languages():
    return (
        RussianLanguage(),
        EnglishLanguage(),
    )


@pytest.fixture
def default_language():
    return RussianLanguage()


def test_check_english(languages):
    language = LanguageDeterminer.get_language('this is english', languages)
    assert isinstance(language, EnglishLanguage)


def test_check_russian(languages):
    language = LanguageDeterminer.get_language('это русский', languages)
    assert isinstance(language, RussianLanguage)


def test_check_unknown(languages):
    language = LanguageDeterminer.get_language('это русский and english', languages)
    assert language is None


def test_check_default(languages, default_language):
    language = LanguageDeterminer.get_language_with_fallback('это русский and english', languages, default_language)
    assert isinstance(language, RussianLanguage)
