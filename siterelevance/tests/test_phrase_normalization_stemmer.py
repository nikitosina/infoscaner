import pytest

from siterelevance.model.language import EnglishLanguage, RussianLanguage
from siterelevance.model.phrase_normalization_stemmer import PhraseNormalizationStemmer


@pytest.fixture
def languages():
    return (
        RussianLanguage(),
        EnglishLanguage(),
    )


@pytest.fixture
def default_language():
    return RussianLanguage()


def test_stemmer_russian(languages, default_language):
    stemmer = PhraseNormalizationStemmer(languages, default_language)
    words = ('привет', 'желтый', 'ходит', 'мне')

    for w in words:
        result = stemmer.process(w)
        assert result in w


def test_stemmer_english(languages, default_language):
    stemmer = PhraseNormalizationStemmer(languages, default_language)
    words = ('hello', 'plays', 'following', 'follower')

    for w in words:
        result = stemmer.process(w)
        assert result in w


def test_stemmer_phrase(languages, default_language):
    stemmer = PhraseNormalizationStemmer(languages, default_language)
    phrase = 'плывет широкий building knows плавает звать'

    result = stemmer.process(phrase)

    for word_in, word_out in zip(phrase.split(), result.split()):
        assert word_out in word_in
