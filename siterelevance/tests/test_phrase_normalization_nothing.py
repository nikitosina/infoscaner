from siterelevance.model.phrase_normalization_nothing import PhraseNormalizationNothing


def test_normalization_nothing():
    normalizer = PhraseNormalizationNothing()

    phrase = 'this фраза stays неизменной'

    result = normalizer.process(phrase)

    assert phrase == result
