import pytest

from siterelevance.model.language import EnglishLanguage, RussianLanguage
from siterelevance.model.stop_words_removal import StopWordsRemoval


@pytest.fixture
def languages():
    return (
        RussianLanguage(),
        EnglishLanguage(),
    )


@pytest.fixture
def default_language():
    return RussianLanguage()


def test_stop_words_removal_russian(languages, default_language):
    remover = StopWordsRemoval(languages, default_language)

    phrase = 'В Этом    Тесте  мноГо пРоверок, не так ли? Превышает 1.5, но не превосходит 3000'

    expected = 'тесте проверок превышает 1.5 превосходит 3000'
    result = remover.remove(phrase)

    assert result == expected


def test_stop_words_removal_english(languages, default_language):
    remover = StopWordsRemoval(languages, default_language)

    phrase = 'The TesT   is   FULL of  checks, right? More than 2.89 in partiCULAr'

    expected = 'test full checks right 2.89 particular'
    result = remover.remove(phrase)

    assert result == expected
