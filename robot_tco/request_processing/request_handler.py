import datetime
import http.client
import traceback
import urllib.error
from multiprocessing import Process, Queue

import pandas as pd
from pymongo import MongoClient
from scrapy.crawler import CrawlerRunner
from twisted.internet import reactor

from attachments.output_attachment_handler import OutputAttHandler
from helpers import url_helper
from attachments.input_attachment_handler import InputAttHandler
from helpers.errors_helper import add_to_err_list_and_logs
from helpers.url_helper import url_cleaner, url_decode
from nlp.process_data import SiteTextProcessor
from scraping.tco_spider import TcoSpider
from settings import *


class RequestHandler:
    """Класс :class:`~RequestHandler` служит для обработки пользовательский запросов с помошью
    таких классов, как :class:`~robot_tco.scraping.tco_spider.TcoSpider` и
    :class:`~nlp.process_data.SiteTextProcessor`.

    Attributes:
        msg (dict): Словарь, содержащих информацию о сообщении.
        logger (~logs.logs_helper.Logger): Логгер данного пользовательского обращения.
        error_list (list): Список ошибок сообщения.
        requests_coll (MongoDBCollection): Коллекция документов пользовательских
            обращений в базе данных.
        organisations_coll (MongoDBCollection): Коллекция документов организаций в базе данных.

    Args:
        msg (dict): Словарь, содержащих информацию о сообщении.
        logger (~logs.logs_helper.Logger): Логгер данного пользовательского обращения.
        error_list (list): Список ошибок сообщения.
        requests_coll (MongoDBCollection): Коллекция документов пользовательских
            обращений в базе данных.
        organisations_coll (MongoDBCollection): Коллекция документов организаций в базе данных.
    """

    def __init__(self, msg, logger, requests_coll, organisations_coll):
        self.msg = msg
        self.logger = logger
        self.error_list = self.msg['errors']
        self.requests_coll = requests_coll
        self.organisations_coll = organisations_coll

    def parse_attachment(self):
        """Метод, вызывающий создающий экземпляр класса :class:`~attachment_helper.AttHelper`
        и читающий вложенный файл методом
        :func:`~attachment_helper.AttHelper.get_attachment_content`.

        Returns:
            dict: Словарь с контентом вложенного файла, разделенный на информацию об организациях
            и информацию о ключевых словах.
        """
        attachment_helper = InputAttHandler(msg=self.msg, logger=self.logger)
        attachment_content = attachment_helper.get_attachment_content()
        return attachment_content

    def site_connections_processing(self, organisations: pd.DataFrame):
        """Метод, служащий для поочередной проверки подключения и правильности URL сайтов,
        указанных в запросе пользователя.

        Args:
           organisations (dict): Информация об организациях из вложенного файла сообщения.

        Returns:
            tuple: Кортеж, содержащий:

                **list**: Список словарей, которые содержат информацию о сайтах,
                указынных в запросе.

                **list**: Список сайтов к которым удалось подключиться.
        """
        site_list = []

        already_inserted_urls = []

        redirects_list = {}
        for index, row in organisations.iterrows():
            org_id = row['ИНН Организации']
            original_org_url = row['Адрес сайта организации']
            org_name = row['Наименование организации']
            cleaned_org_url = original_org_url
            redirected_url = None
            try:
                cleaned_org_url = url_decode(url_cleaner(url=original_org_url))
                redirected_url = url_helper.check_redirect(url=original_org_url)

                connection_status = 200
            except UnicodeError as err:
                add_to_err_list_and_logs(exception_info={'type': 'url_unicode',
                                                         'site_url': original_org_url},
                                         err_list=self.msg['errors'],
                                         logger=self.logger)
                connection_status = 400
                pass
            except (urllib.error.URLError, ConnectionError, http.client.RemoteDisconnected) as err:
                exception_info = {'type': 'url_connection',
                                  'original_org_url': original_org_url,
                                  'cleaned_org_url': cleaned_org_url,
                                  'repr_err': repr(err),
                                  'err_traceback': ''.join(traceback.format_tb(err.__traceback__))}
                add_to_err_list_and_logs(exception_info=exception_info,
                                         err_list=self.msg['errors'],
                                         logger=self.logger)
                connection_status = 500
                pass
            except Exception as err:
                print(original_org_url)
                print(repr(err))
                print()

                exception_info = {'type': 'url_unknown',
                                  'original_org_url': original_org_url,
                                  'cleaned_org_url': cleaned_org_url,
                                  'repr_err': repr(err),
                                  'err_traceback': ''.join(traceback.format_tb(err.__traceback__))}
                add_to_err_list_and_logs(exception_info=exception_info,
                                         err_list=self.msg['errors'],
                                         logger=self.logger)
                connection_status = 404
                pass

            if redirected_url:
                redirects_list.update({original_org_url: redirected_url})
                org_domain = url_helper.get_domain(redirected_url)
                self.logger.add_log('Сайт организации "{}" был перенаправлен на сайт "{}"'
                                    .format(original_org_url, redirected_url))
            else:
                org_domain = url_helper.get_domain(cleaned_org_url)

            url_lang = url_helper.define_lang(cleaned_org_url)
            site_final_url = cleaned_org_url if not redirected_url else redirected_url

            # Избаление от дубликатов сайтов в запросе
            # Не считает дубликатами сайты с www и без www
            if site_final_url not in already_inserted_urls:
                already_inserted_urls.append(site_final_url)
                site_list.append({'status': connection_status,
                                  'id': org_id,
                                  'name': org_name,
                                  'original_url': original_org_url,
                                  'cleaned_url': cleaned_org_url,
                                  'redirected_url': redirected_url,
                                  'domain': org_domain,
                                  'url_language': url_lang})

            else:
                add_to_err_list_and_logs(exception_info={'type': 'duplicated_org',
                                                         'dup_item': site_final_url},
                                         err_list=self.msg['errors'],
                                         logger=self.logger)

        # Сайты, к которым удалось подключиться
        active_sites = [site for site in site_list if site['status'] == 200]
        self.logger.add_log('Сайтов организаций до фильтрации: {}. После: {}'
                            .format(len(site_list), len(active_sites)))

        return site_list, active_sites

    def create_user_document(self, status, site_list, keywords):
        """Метод, для создания документа, соответствующего запросу пользователя,
        который в дальнейшем будет направлен в коллекию ``requests_coll``.

        Args:
            status (dict): Словарь, содержащих информацию о сообщении.
            site_list (list): Список словарей, которые содержат информацию о сайтах,
                указынных в запросе.
            keywords (dict): Словарь, содержащий информацию о ключевых словах данного запроса.

        Returns:
            user_document (dict): Словарь, соответствующий документу, который подлежит
            загрузке в коллекцию ``requests_coll``.
        """
        user_document = {'status': status,
                         'from': self.msg['from'],
                         'subject': self.msg['subject'],
                         'received_time': self.msg['received_time'],
                         'handled_time': datetime.datetime.now(),
                         'site_list': site_list,
                         'keywords': keywords,
                         'error_list': self.msg['errors']}
        return user_document

    def parse_request(self, attachment_content):
        """Метод, сначала проверяющий подключение к сайтам через
        :func:`~site_connections_processing`, а потом, составляющий документы через
        func:`~create_user_document`, который затем загружаются в ``requests_coll``

        Args:
             attachment_content (dict): Словарь с контентом вложенного файла, разделенный на
                информацию об организациях и информацию о ключевых словах.

        Returns:
            bson.ObjectId: Идентификатор документа, загруженного в ``requests_coll``.
        """
        if attachment_content:
            organisations_df = attachment_content['organisations']
            search_keywords = attachment_content['search_keywords']
            site_list, active_sites = self.site_connections_processing(organisations_df)

            if not active_sites:
                add_to_err_list_and_logs(exception_info={'type': 'no_sites_connected'},
                                         err_list=self.msg['errors'],
                                         logger=self.logger)

                user_document = self.create_user_document(status='error',
                                                          site_list=site_list,
                                                          keywords=search_keywords)
            else:
                user_document = self.create_user_document(status='ok',
                                                          site_list=site_list,
                                                          keywords=search_keywords)
                self.logger.add_log('Адреса сайтов успешно собраны.'
                                    ' Начинается сраппинг сайтов и составление корпусов.')
        else:
            user_document = self.create_user_document(status='error',
                                                      site_list=None,
                                                      keywords=None)

        user_request_id = self.requests_coll.insert_one(user_document).inserted_id
        return user_request_id

    def update_request_scraped_sites(self, user_request_id, site_list):
        """Метод, дополняющий документ пользовательского запроса в базе данных списком сайтов,
        для которых скрейпинг прошел успешно.

        Args:
            user_request_id: Идентификатор документа, загруженного в ``requests_coll``.
            site_list (list): Список словарей, которые содержат информацию о сайтах,
                указынных в запросе.
        """
        scraped_sites = []
        for site in site_list:
            site_domain = site['domain']
            successfully_scraped = self.organisations_coll.find_one({'domain': site_domain})
            if successfully_scraped:
                scraped_sites.append(site_domain)
        self.requests_coll.update_one({'_id': user_request_id},
                                      {'$set': {'scraped_domains': scraped_sites}},
                                      upsert=False)

    @staticmethod
    def crawl_site(url, domain):
        """Метод, запускающий отдельные процессы, реализующие скрейпинг
        :class:`~robot_tco.scraping.tco_spider.TcoSpider`

        Args:
            url (str): URL адрес сайта, который необходимо скрейпить.
            domain (str): Домен сайта, который необходимо скрейпить.
        """
        def f(queue):
            try:
                # Для каждого процесса нужно создавать отдельный клиент Mongo
                # client = MongoClient(host=MONGO_HOST_TEST, port=MONGO_PORT_TEST)
                client = MongoClient(host=MONGO_HOST,
                                     port=MONGO_PORT,
                                     username=MONGO_USERNAME,
                                     password=MONGO_PASSWORD,
                                     authSource=MONGO_AUTH_SOURCE,
                                     authMechanism=MONGO_AUTH_MECH)
                robot_tco_db = client['robot_tco']
                organisations_coll = robot_tco_db.organisations
                runner = CrawlerRunner(settings={"LOG_ENABLED": False})
                deferred = runner.crawl(crawler_or_spidercls=TcoSpider,
                                        urls=[url],
                                        allowed_domains=[domain],
                                        organisations_coll=organisations_coll)
                deferred.addBoth(lambda _: reactor.stop())
                reactor.run()
                queue.put(None)
            except Exception as e:
                queue.put(e)

        q = Queue()
        p = Process(target=f, args=(q,))
        p.start()
        result = q.get()
        p.join()

        if result is not None:
            raise result

    def start_scraping(self, user_request_id):
        """Метод, реализующий работу :class:`~robot_tco.scraping.tco_spider.TcoSpider` через
        :func:`~crawl_site` и впоследствии обновляющей документы в ``requests_coll`` путем вызова
        :func:`~update_request_scraped_sites`.

        Args:
            user_request_id (bson.ObjectId): Идентификатор документа,
                загруженного в ``requests_coll``.
        """
        user_request = self.requests_coll.find_one({'_id': user_request_id,
                                                    'status': 'ok'})
        user_request = user_request if user_request else {}
        unfiltered_sites = user_request.get('site_list', [])

        site_list = [site for site in unfiltered_sites if site['status'] == 200]
        organisations_to_scrape = []
        for site in site_list:
            site_url = site['cleaned_url'] if \
                site['redirected_url'] is None else site['redirected_url']
            site_domain = site['domain']
            organisations_to_scrape.append({'url': site_url,
                                           'domain': site_domain})
            try:
                self.crawl_site(url=site_url, domain=site_domain)
                self.logger.add_log('С сайта "{}" успешно собрана информация.'.format(site_domain))
            except Exception as err:
                exception_info = {'type': 'scraping_err',
                                  'domain': site_domain,
                                  'repr_err': repr(err),
                                  'err_traceback': ''.join(traceback.format_tb(err.__traceback__))}
                add_to_err_list_and_logs(exception_info=exception_info,
                                         err_list=self.msg['errors'],
                                         logger=self.logger)

        self.update_request_scraped_sites(user_request_id=user_request_id, site_list=site_list)

    def find_relevance(self, user_request_id):
        """Метод, реализующий работу :class:`~nlp.process_data.SiteTextProcessor` на основе сайтов
        из запроса пользователя, для которых скрейпинг прошел успешно.

        Returns:
            bytearray: Набор байт, соответствующий ответному вложенному файлу в формате **excel**.

        Args:
            user_request_id (bson.ObjectId): Идентификатор документа,
                загруженного в ``requests_coll``.
        """
        try:
            text_processor = SiteTextProcessor(user_request_id=user_request_id,
                                               requests=self.requests_coll,
                                               organisations=self.organisations_coll,
                                               user_logger=self.logger)
            scraped_sites = text_processor.get_scraped_sites()
            if scraped_sites:
                keywords = text_processor.get_keywords()
                if not keywords.empty:
                    rel_list = text_processor.find_relevance_tables(scraped_sites, keywords)
                    out_att_helper = OutputAttHandler(user_request_id=user_request_id, user_logger=self.logger,
                                                      requests=self.requests_coll, organisatins=self.organisations_coll)
                    output_file = out_att_helper.create_ouput_att(rel_list)
                    return output_file
        except Exception as err:
            exception_info = {'type': 'process_data',
                              'repr_err': repr(err),
                              'err_traceback': ''.join(traceback.format_tb(err.__traceback__))}
            add_to_err_list_and_logs(exception_info=exception_info,
                                     err_list=self.msg['errors'],
                                     logger=self.logger)
