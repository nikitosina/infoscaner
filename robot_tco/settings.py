MONGO_HOST = '79.174.70.192'
MONGO_PORT = 32412
MONGO_USERNAME = 'mongo-root'
MONGO_PASSWORD = 'J*f0-wb&8c'
MONGO_AUTH_SOURCE = 'admin'
MONGO_AUTH_MECH = 'SCRAM-SHA-1'
MONGO_HOST_TEST = 'localhost'
MONGO_PORT_TEST = 27017

N_GRAM_MAX = 3
CRITERIA_RANGE = ['v', 's', 'n']
WEIGHT_RANGE = {'min_weight': 0.0,
                'max_weight': 10.0,
                'zeroing_out_weight': -1.0}
RU_REGEX = '^[а-яА-Я0-9-\s]+$'
EN_REGEX = '^[a-zA-Z0-9-\s]+$'
