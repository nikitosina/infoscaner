import ssl
import urllib
from pprint import pprint
import pandas as pd
from urllib.error import URLError
from urllib.parse import urljoin
from urllib.request import urlopen

import xlsxwriter
import html5lib
from bs4 import BeautifulSoup

from smartsites_2_0.robot_tco.helpers.about_page_helper import *


# site = 'https://ymioil.ru/'
# site = 'http://бзпк.рф/'
# site = 'http://revoll.ru/'
# site = 'https://himavto.com/'

sites = ['http://nanotek.ru']

about_dict = {}

for site in sites:
    try:
        site_about = get_about_text(site_url=site)
        pprint(site_about)
    except (UnicodeEncodeError, TypeError, urllib.error.URLError, ConnectionResetError):
        continue
