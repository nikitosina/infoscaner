import datetime

from pymongo import MongoClient
from scrapy import Request
from scrapy.crawler import CrawlerProcess
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider

from settings import MONGO_HOST_TEST, MONGO_PORT_TEST
from spider_helpers import *

site_list = []
links_set = set()
client = MongoClient(host=MONGO_HOST_TEST, port=MONGO_PORT_TEST)
robot_tco_db = client['robot_tco']


class TcoSpider(CrawlSpider):
    name = 'tco_spider'

    allowed_domains = []
    urls = []

    scraped_count = 0
    limit = 50

    visited_urls = []
    pages_text_list = []

    domain = ''

    organisations_coll = robot_tco_db.organisations_coll

    def start_requests(self):
        """Метод ``start_requests``
        генерирует запросы в виде объектов класса :class:`scrapy.Request`
        по списку urls, содержащимуся в атрибуте класса ``TcoSpider``
        """
        self.domain = self.allowed_domains[0]
        if check_domain_in_db(self.organisations_coll, self.domain):
            for url in self.urls:
                yield Request(url=url, callback=self.parse)

    def parse(self, response, **kwargs):
        links = LinkExtractor(allow_domains=self.domain).extract_links(response)
        for link in links:
            if link.url not in self.visited_urls and self.scraped_count < self.limit:
                self.visited_urls.append(link.url)
                # print(self.scraped_count, link.url)
                page_text = {'link': link.url,
                             'text': get_text_from_html(response.text)}
                self.pages_text_list.append(page_text)
                self.scraped_count += 1
                yield Request(link.url, callback=self.parse)

    def closed(self, spider):
        valid_pages_text_list = validate_size_of_list(self.pages_text_list)
        if valid_pages_text_list:
            insert_in_db(organisations_coll=self.organisations_coll,
                         pages_texts=valid_pages_text_list,
                         domain=self.domain)


process = CrawlerProcess(settings={"LOG_ENABLED": False})
process.crawl(TcoSpider, urls=['https://alekogroup.ru/'],
              allowed_domains=['alekogroup.ru'])
process.start()
