import datetime
import sys

import html5lib
from bs4 import BeautifulSoup


def get_text_from_html(html):
    tree = BeautifulSoup(html, 'html5lib')
    body = tree.body

    if body is None:
        return ''

    for tag in body.select('script'):
        tag.decompose()
    for tag in body.select('style'):
        tag.decompose()

    text = body.get_text(separator=' ')
    text = ' '.join(text.replace('\n', '').split()).lower()
    return text


def validate_size_of_list(pages_text_list):
    curr_size = sum(map(sys.getsizeof, pages_text_list))
    # Ставим ограничение в 15МБ, так как в MongoDB ограничение на 16МБ на документ
    while curr_size > 1500000:
        pages_text_list.pop()
        curr_size = sum(map(sys.getsizeof, pages_text_list))
    return pages_text_list


def check_domain_in_db(organisations_coll, domain):
    organisation = organisations_coll.find_one({'domain': domain})
    if organisation:
        time_of_one_week = datetime.timedelta(weeks=1)

        updated_time = organisation['updated']
        current_time = datetime.datetime.now()
        time_diff = current_time - updated_time

        # Если прошло больше недели
        if time_diff >= time_of_one_week:
            should_be_inserted = True
        else:
            should_be_inserted = False
    else:
        should_be_inserted = True
    return should_be_inserted


def insert_in_db(organisations_coll, pages_texts, domain):
    organisations_coll.update_one({'domain': domain},
                                  {'$setOnInsert': {'created': datetime.datetime.now()},
                                   '$set': {'updated': datetime.datetime.now(),
                                            'pages_texts': pages_texts}},
                                  upsert=True)
