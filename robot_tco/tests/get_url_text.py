import html5lib
from bs4 import BeautifulSoup
from pprint import pprint


def get_text_from_html(html):
    tree = BeautifulSoup(html, 'html5lib')
    body = tree.body

    if body is None:
        return ''

    for tag in body.select('script'):
        tag.decompose()
    for tag in body.select('style'):
        tag.decompose()

    text = body.get_text(separator=' ')
    text = ' '.join(text.replace('\n', '').split()).lower()
    return text


# with open('text2.html', 'r') as file:
#     html_text = file.read()
#
# body_text = get_text_from_html(html_text)
#
# pprint(body_text)
