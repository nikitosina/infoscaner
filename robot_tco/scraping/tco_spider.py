from scrapy import Request
from scrapy.exceptions import CloseSpider
from scrapy.linkextractors import LinkExtractor
from scrapy.spidermiddlewares.httperror import HttpError
from scrapy.spiders import CrawlSpider, Rule
from twisted.internet.error import DNSLookupError, TCPTimedOutError

from helpers import about_page_helper
from helpers.url_helper import url_cleaner, url_decode, get_domain
from scraping.spider_helpers import *


class TcoSpider(CrawlSpider):
    """Класс :class:`~TcoSpider`, унаследованный от класса :class:`scrapy.spiders.CrawlSpider`.
    Используется для скрейпинга сайтов из списка.

    Attributes:
        allowed_domains (list): Список доменов за которые паук не может выбраться.
        urls (list): Список адресов сайтов для скрейпинга.
        start_urls (list): Список преобразованных адресов сайтов для скрейпинга.
        scraped_count (int): Текущее кол-во просмотренных страниц.
        scraped_limit (int): Макс. кол-во просмотренных страниц
        unique_count (int): Текущее кол-во уникальных текстовых содержимых страниц.
        unique_limit (int): Макс. кол-во уникальных текстовых содержимых страниц.
        failed_count (int): Текущее кол-во неудачно просметренных страниц.
        failed_limit (int): Макс. кол-во неудачно просметренных страниц.
        visited_urls (list): Список посещенных страниц.
        pages_texts (list): Список содержимых сайтов.
        unique_texts (list): Список уникальных содержимых сайтов.
        domain (str): Домен в пределах которого происходит скрейпинг.
        organisations_coll (MongoDBCollection): Коллекция организаций MongoDB.
    """
    name = 'tco_spider'

    allowed_domains = []
    urls = []
    start_urls = []

    scraped_count = 0
    scraped_limit = 200

    unique_count = 0
    unique_limit = 30

    failed_count = 0
    failed_limit = 10

    visited_urls = []
    pages_texts = []
    unique_texts = []

    domain = ''

    organisations_coll = None

    def __init__(self, *args, **kwargs):
        super(TcoSpider, self).__init__(*args, **kwargs)

        self.domain = get_domain(self.urls[0])
        self.start_urls = [url_cleaner(self.urls[0])]
        self.start_url = self.start_urls[0]
        self.allowed_domains = [get_domain(self.start_urls[0])]

        if check_domain_in_db(self.organisations_coll, self.domain):
            TcoSpider.rules = (Rule(LinkExtractor(allow=(self.allowed_domains[0],), ),
                                    callback="parse",
                                    follow=True),)
            super(TcoSpider, self)._compile_rules()

        else:
            pass

    def parse(self, response, **kwargs):
        """Метод :func:`~parse` испульзуемый, как параметр
        для атрибута callback класса :class:`scrapy.Request`, позволяет установить
        по каким правилам будет происходить сбор текстовых данных со страниц.

        Args:
            response (scrapy.http.Response): Ответ на запрос, сделанный объектом
                класса scrapy.Request.
            **kwargs: Именованные аргументы.
        """
        if response.url not in self.visited_urls:
            if self.scraped_count < self.scraped_limit and self.unique_count < self.unique_limit:
                self.visited_urls.append(response.url)
                page_text = {'link': url_decode(response.url),
                             'text': get_text_from_html(response.text)}
                if page_text['text'] not in self.unique_texts:
                    self.unique_texts.append(page_text['text'])
                    self.pages_texts.append(page_text)
                    self.unique_count += 1
                self.scraped_count += 1
                yield Request(response.url, callback=self.parse, errback=self.err_parse)
            else:
                raise CloseSpider

    def err_parse(self, failure):
        """Метод :func:`~err_parse` испульзуемый, как параметр
        для атрибута errback класса :class:`scrapy.Request`, позволяет установить
        по каким правилам будет происходить обработка ошибок.

        Args:
            failure (twisted.python.failure.Failure): Ошибка, возвращенная на запрос,
                сделанный объектом класса scrapy.Request.

        Ошибки бывают тех типов: HttpError, DNSLookupError и TimeoutError.
        """
        if failure.check(TimeoutError, TCPTimedOutError, HttpError, DNSLookupError):
            self.failed_count += 1
            if self.failed_count >= self.failed_limit:
                raise CloseSpider

    def closed(self, spider):
        """Метод :func:`~closed` позволяет останавливать скрейпинг и вносить изменения
        в коллекцию ``organisations_coll`` при завершении обхода страниц сайта или при достижении
        одного из лимитов: ``scraped_limit``, ``unique_limit``, ``failed_limit``.

        Args:
            spider (scrapy.spiders.CrawlSpider): Экземпляр класса CrawlSpider.
        """
        valid_pages_text_list = validate_size_of_list(self.pages_texts)
        if valid_pages_text_list:
            try:
                about_content = about_page_helper.get_about_text(site_url=self.start_url)
            except:
                about_content = {'about_page': 'Ошибка при поиске описания сайта', 'about_text': ''}
            insert_in_db(organisations_coll=self.organisations_coll,
                         pages_texts=valid_pages_text_list,
                         domain=self.domain,
                         about_content=about_content)
        spider.crawler.engine.close_spider(self, reason='finished')
