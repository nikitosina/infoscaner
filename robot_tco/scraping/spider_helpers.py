import datetime
import sys

import html5lib
from bs4 import BeautifulSoup


def get_text_from_html(html):
    """Функция, вычленяющаяя весь текст из внутри тега **body** html документа. А также, приводящая
    его к нижнему регистру.

    Args:
        html (str): Содержимое html документа.

    Returns:
        str: Преобразованный текст страницы.
    """
    tree = BeautifulSoup(html, 'html5lib')
    body = tree.body

    if body is None:
        return ''

    for tag in body.select('script'):
        tag.decompose()
    for tag in body.select('style'):
        tag.decompose()

    text = body.get_text(separator=' ')
    text = ' '.join(text.replace('\n', '').split()).lower()
    return text


def validate_size_of_list(pages_text_list):
    """Фукнция служит для проверки веса(в байтах), накопленного после скрейпинга страниц домена.
    Вес не должен превышать 15Мб, так как максимальный допустимый вес для документа в
    MongoDB - 16Мб, а по мимо содержимого веб-сайтов документ в базе должен
    содеражить доп. информацию. Если документ превышает макс. допустимый вес, то с конца удаляется
    по одному элементу, пока вес текстовой информации не достингет допустимых значений.

    Args:
        pages_text_list (list): Список с текстовым содержимым страниц домена.

    Returns:
        list: Список, с текстовой информвцией, не превышающей 15Мб.
    """
    curr_size = sum(map(sys.getsizeof, pages_text_list))
    # Ставим ограничение в 15МБ, так как в MongoDB ограничение на 16МБ на документ
    while curr_size > 1500000:
        pages_text_list.pop()
        curr_size = sum(map(sys.getsizeof, pages_text_list))
    return pages_text_list


def check_domain_in_db(organisations_coll, domain):
    """Функция, позволяющая проверить наличие домена в базе, а также время последнего скрейпинга
    по данному домену, если он присутствует. Если разница в текущем времени и времени
    последнейго скрейпинга превышает одну неделю, тогда информация об этом домене перезаписывается.

    Args:
          organisations_coll (MongoDBCollection): Коллекция организаций MongoDB.
          domain (str): Домен, для которого происходит проверка.

    Returns:
        bool: ``True``, если информацию о домене следует перезаписать, ``False``,
        если менять нечего.
    """
    organisation = organisations_coll.find_one({'domain': domain})
    if organisation:
        time_of_one_week = datetime.timedelta(weeks=1)

        updated_time = organisation['updated']
        current_time = datetime.datetime.now()
        time_diff = current_time - updated_time

        # Если прошло больше недели
        if time_diff >= time_of_one_week:
            should_be_inserted = True
        else:
            should_be_inserted = False
    else:
        should_be_inserted = True
    return should_be_inserted


def insert_in_db(organisations_coll, pages_texts, domain, about_content):
    """Функция, позволяющая внести информацию о домене в базу данных.

    Args:
          organisations_coll (MongoDBCollection): Коллекция организаций MongoDB.
          pages_texts (list): Cписок, с текстовой информвцией, не превышающей 15Мб.
          domain (str): Домен, для которого происходит проверка.
          about_content (str): Текст страницы с описанием деятельности организации.
    """
    organisations_coll.update_one({'domain': domain},
                                  {'$setOnInsert': {'created': datetime.datetime.now()},
                                   '$set': {'updated': datetime.datetime.now(),
                                            'pages_texts': pages_texts,
                                            'raw_word_relevance': {},
                                            'about_content': about_content}},
                                  upsert=True)
