from scrapy.crawler import CrawlerProcess
from scraping.tco_spider import TcoSpider

runner = CrawlerProcess(settings={"LOG_ENABLED": False})
runner.crawl(crawler_or_spidercls=TcoSpider,
             urls=['http://gremlos.ru/'],
             allowed_domains=['gremlos.ru'],
             organisations_coll=None)
runner.start()