import traceback

from pymongo import MongoClient

from exceptions.exceptions import ConnctionToMailError
from settings import *
from logs.logs_helper import Logger
from mail.mailbox_manager import MailBoxManager
from request_processing.request_handler import RequestHandler
from time import time, sleep

def work():
    try:
        client = MongoClient(host=MONGO_HOST,
                             port=MONGO_PORT,
                             username=MONGO_USERNAME,
                             password=MONGO_PASSWORD,
                             authSource=MONGO_AUTH_SOURCE,
                             authMechanism=MONGO_AUTH_MECH)
        robot_tco_db = client['robot_tco']
        user_requests = robot_tco_db.user_requests
        organisations = robot_tco_db.organisations
    except Exception as err:
        print(f'DB Connection Error: {repr(err)},'
              f' Details: {"".join(traceback.format_tb(err.__traceback__))}')
    else:
        logger = None
        try:
            logger = Logger(client)
            mailbox_manager = MailBoxManager(logger=logger, requests_coll=user_requests)

            try:
                mailbox_manager.connect_mailbox()
            except ConnctionToMailError:
                raise ConnctionToMailError

            msgs = mailbox_manager.get_messages(move_to_archive=True)
            msgs_count = len(msgs)

            for msg_index, msg in enumerate(msgs):
                user_logger = logger.copy(from_=msg['from'], subj_=msg['subject'], received_time=msg['received_time'])
                user_logger.add_log(
                    'Обработка сообщения {} из {}'.format(msg_index + 1, msgs_count))

                request_handler = RequestHandler(msg=msg,
                                                 logger=user_logger,
                                                 requests_coll=user_requests,
                                                 organisations_coll=organisations)
                attachment_content = request_handler.parse_attachment()

                user_request_id = request_handler.parse_request(attachment_content=attachment_content)

                request_handler.start_scraping(user_request_id)
                msg['result_attachment'] = request_handler.find_relevance(user_request_id)
                msg_to_send = mailbox_manager.create_msg(msg_content=msg)

                try:
                    mailbox_manager.send_mail(msg_to_send)
                except ConnctionToMailError as err:
                    user_logger.add_log('Ошибка во время отправки сообщения пользователю: {}.'
                                        ' Детали: {}.'
                                        .format(repr(err),
                                                "".join(traceback.format_tb(err.__traceback__))))
                finally:
                    user_logger.push_logs(from_=msg['from'],
                                          subj_=msg['subject'],
                                          user_request_id=user_request_id,
                                          received_time=msg['received_time'])

        except Exception as err:
            log = f'Error during work: {repr(err)}, ' \
                  f'Details: {"".join(traceback.format_tb(err.__traceback__))}'
            if logger:
                logger.add_log(log)
                logger.push_logs(from_='system', subj_='system_error')


if __name__ == '__main__':
    while True:
        work()
        sleep(60 - time() % 60)

