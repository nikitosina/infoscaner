import re
import traceback

import pandas as pd
import numpy as np
import xlrd

from helpers.errors_helper import add_to_err_list_and_logs
from settings import CRITERIA_RANGE, WEIGHT_RANGE
from exceptions.exceptions import ValidationError


class InputAttHandler:
    """Класс :class:`~AttHelper` служит для обработки вложенный в email excel файлов.

    Attributes:
        msg (dict): Содержимое сообщения
        logger (~logs.logs_helper.Logger): Логер, позволяющий сохранять информацию
            о выполнении действий с сообщением
    """

    def __init__(self, msg, logger):
        self.msg = msg
        self.logger = logger

    def get_attachment_content(self):
        """Метод, позволяющих выделить обработать вложенный файл сообщения ``msg``. Путем:

            1. Преобразования **excel** файла к датафрейму.
            2. Деления этого датафрейма на две части.
            3. Проверки и преобразования каждого из двух датафреймов.
            4. Создания словаря, содержащего два преобразованных датафрейма, для дальнейшей загрузки
               его в базу данных.

        Returns:
            dict: Словарь вида:

                **organisations**: Информация об организациях из вложенного файла сообщения.

                **search_keywords**: Информация о ключевых словах из вложенного файла сообщения.
        """
        input_attachment = self.msg['input_attachment']
        try:
            df = pd.read_excel(input_attachment, header=6,
                               sheet_name='Запросы', dtype=str, keep_default_na=False).iloc[:, 1:7]
            df = df.rename(columns={
                "Вспомогательная информация по сайту 1": "ИНН Организации",
                "Вспомогательная информация по сайту 2": "Наименование организации",
                "Адрес сайта": "Адрес сайта организации",
                "Ключевые слова для оценки поиска": "Ключевые слова",
                "Критерий поиска ключевого слова": "Критерий поиска ключевого слова",
                "Вес ключевого слова": "Вес ключевого слова",
            })

            required_column_names = ['ИНН Организации',
                                     'Адрес сайта организации',
                                     'Наименование организации',
                                     'Ключевые слова',
                                     'Критерий поиска ключевого слова',
                                     'Вес ключевого слова'].sort()

            if required_column_names != list(df.columns).sort():
                raise ValidationError

            organisations = self.get_organisations_df(df)
            search_keywords = self.get_search_keywords(df)

            organisations = self.drop_duplacates(df=organisations,
                                                 column_name='Адрес сайта организации',
                                                 exception_type='duplicated_org')

            search_keywords = self.drop_duplacates(df=search_keywords,
                                                   column_name='Ключевые слова',
                                                   exception_type='duplicated_keyword')

            search_keywords = self.fix_keywords(df=search_keywords)

            self.logger.add_log('Вложенный файл успешно прочитан.')

            return {'organisations': organisations, 'search_keywords': search_keywords}

        except xlrd.biffh.XLRDError as err:
            exception_info = {'type': 'att_xlrd_err',
                              'repr_err': repr(err),
                              'err_traceback': ''.join(traceback.format_tb(err.__traceback__))}
            add_to_err_list_and_logs(exception_info=exception_info,
                                     err_list=self.msg['errors'],
                                     logger=self.logger)

        except ValidationError as err:
            exception_info = {'type': 'att_validation_err',
                              'err_msg': err,
                              'repr_err': repr(err),
                              'err_traceback': ''.join(traceback.format_tb(err.__traceback__))}
            add_to_err_list_and_logs(exception_info=exception_info,
                                     err_list=self.msg['errors'],
                                     logger=self.logger)

        except Exception as err:
            exception_info = {'type': 'att_validation_err',
                              'repr_err': repr(err),
                              'err_traceback': ''.join(traceback.format_tb(err.__traceback__))}
            add_to_err_list_and_logs(exception_info=exception_info,
                                     err_list=self.msg['errors'],
                                     logger=self.logger)

    def drop_duplacates(self, df, column_name, exception_type):
        result_df = df.copy()
        duplicated_mask = df[column_name].duplicated()

        for index, row in result_df.loc[duplicated_mask].iterrows():
            add_to_err_list_and_logs(exception_info={'type': exception_type,
                                                     'dup_item': row[column_name]},
                                     err_list=self.msg['errors'],
                                     logger=self.logger)
        result_df = result_df.loc[~duplicated_mask]
        return result_df

    @staticmethod
    def get_organisations_df(input_df):
        """**Статический** метод, который строит :class:`~pd.DataFrame` из части вложенного файла,
        в которой содержится информациою о полях:

            - **ИНН Организации**
            - **Адрес сайта организации**
            - **Наименование организации**

        Данные колонки датафрейма проходят через фильтрацию и валидацию.

        Args:
            input_df (pd.DataFrame): Изначальный датафрейм вложенного файла.

        Returns:
            pd.DataFrame: Датафрейм, соответствующий организациям, указанным во вложенном файле.

        Raises:
            :class:`ValidationError`: Ошибка валидации датафрема огранизаций.
        """
        organisations = input_df[['ИНН Организации',
                                  'Адрес сайта организации',
                                  'Наименование организации']]

        # Очистка пустых строк excel таблицы и строк не содержаших ссылку

        organisations = organisations.loc[(organisations.sum(axis=1) != '')
                                          & (organisations['Адрес сайта организации'] != '')]

        # Валидация кол-ва организаций для проверки
        sites_number = len(organisations.index)
        if sites_number > 100:
            raise ValidationError('Слишком много сайтов для проверки {},'
                                  ' максимальное значение: 100'.format(sites_number))
        elif sites_number == 0:
            raise ValidationError('Не нашлось ни одного сайта для проверки')

        return organisations

    @staticmethod
    def get_search_keywords(input_df):
        """**Статический** метод, который строит :class:`~pd.DataFrame` из части вложенного файла,
        в которой содержится информациою о полях:

            - **Ключевые слова**
            - **Критерий поиска ключевого слова**

        Данные колонки датафрейма проходят через фильтрацию и валидацию.

        Args:
            input_df (pd.DataFrame): Изначальный датафрейм вложенного файла.

        Returns:
            pd.DataFrame: Датафрейм, соответствующий ключевым словам, указанным во вложенном файле.

        Raises:
            :class:`ValidationError`: Ошибка валидации датафрема ключевых слов.
        """
        keywords_df = input_df[['Критерий поиска ключевого слова',
                                'Ключевые слова', 'Вес ключевого слова']]

        # Очистка строк excel таблицы, не содержащих параметров ключевых слов
        keywords_df = keywords_df.loc[(keywords_df['Критерий поиска ключевого слова'] != '')
                                      & (keywords_df['Ключевые слова'] != '')]

        # Заполнение пыстых весов
        keywords_df['Вес ключевого слова'] = keywords_df['Вес ключевого слова'] \
            .replace(r'^\s*$', '1.0', regex=True)

        # Валидация кол-ва ключевых слов для проверки
        keywords_number = len(keywords_df.index)
        if keywords_number > 100:
            raise ValidationError('Слишком много ключевых слов для проверки: {},'
                                  ' максимальное значение: 100'.format(keywords_number))
        elif keywords_number == 0:
            raise ValidationError('Не нашлось ни одного ключевого слова')

        return keywords_df

    def check_criteria(self, df):
        """Метод, позволяющий проверить наличие и допустимость **Критерия** у всех
        **Ключевых слов**.

        Args:
            df (pd.DataFrame):  Датафрейм ключевых слов.

        Returns:
            pd.DataFrame: Датафрейм ключевых слов, прошедших проверку.
        """
        result_df = df.copy()
        criteria_is_in_range = result_df['Критерий поиска ключевого слова'].isin(CRITERIA_RANGE)
        for index, row in result_df.loc[~criteria_is_in_range].iterrows():
            add_to_err_list_and_logs(exception_info=
                                     {'type': 'keyword_criteria_err',
                                      'criteria': row['Критерий поиска ключевого слова'],
                                      'word': row['Ключевые слова']},
                                     err_list=self.msg['errors'],
                                     logger=self.logger)
        result_df = result_df.loc[criteria_is_in_range]
        return result_df

    def check_wheight(self, df):
        """Метод, позволяющий проверить допустимость **Веса** у всех
        **Ключевых слов**.

        Args:
            df (pd.DataFrame):  Датафрейм ключевых слов.

        Returns:
            pd.DataFrame: Датафрейм ключевых слов, прошедших проверку.
        """
        result_df = df.copy()
        result_df['Вес ключевого слова'] = result_df['Вес ключевого слова'].astype(np.float)

        weight_is_in_range = (result_df['Вес ключевого слова'] >= WEIGHT_RANGE['min_weight']) & \
                             (result_df['Вес ключевого слова'] <= WEIGHT_RANGE['max_weight']) | \
                             ((result_df['Критерий поиска ключевого слова'] == 's') &
                              (result_df['Вес ключевого слова'] == WEIGHT_RANGE['zeroing_out_weight']))

        for index, row in result_df.loc[~weight_is_in_range].iterrows():
            add_to_err_list_and_logs(exception_info=
                                     {'type': 'weight_range_err',
                                      'weight': row['Вес ключевого слова'],
                                      'word': row['Ключевые слова']},
                                     err_list=self.msg['errors'],
                                     logger=self.logger)

        result_df = result_df.loc[weight_is_in_range]
        return result_df

    @staticmethod
    def define_keyword_lang(word):
        """**Статический** метод, позволяющий отпределить язык ключевого слова.
        Поддерживаются только *ru/en* языки.

        Args:
            word (str):  Ключевых слово.

        Returns:
            str: Обозначение языка.
        """
        ru_regex = '^[а-яА-Я0-9-\s]+$'
        en_regex = '^[a-zA-Z0-9-\s]+$'
        if re.match(ru_regex, word):
            return 'ru'
        elif re.match(en_regex, word):
            return 'en'
        else:
            return 'unknown'

    def check_keywords_lang(self, df):
        """Метод, определяющий язык ключевого слова в датафрейме ключевых слов с помощью
        :func:`~define_keyword_lang`

        Args:
            df (pd.DataFrame): Датафрейм ключевых слов.

        Returns:
            pd.DataFrame: Датафрейм ключевых слов, с дополнительной колонкой языка.
        """
        result_df = df.copy()
        result_df['Язык ключевого слова'] = result_df['Ключевые слова'].apply(
            lambda x: self.define_keyword_lang(x))

        word_is_not_unkwown_bool = result_df['Язык ключевого слова'] != 'unknown'
        for index, row in result_df.loc[~word_is_not_unkwown_bool].iterrows():
            add_to_err_list_and_logs(exception_info=
                                     {'type': 'invalid_keyword_lang',
                                      'word': row['Ключевые слова']},
                                     err_list=self.msg['errors'],
                                     logger=self.logger)
        result_df = result_df.loc[word_is_not_unkwown_bool]
        return result_df

    def check_keyword_length(self, df):
        """Метод, определяющий кол-во слов ключевого слова в датафрейме ключевых слов.
        Ключевые слова, состоящих из более, чем 3 слов выбрасываются из датафрейма. Это сделано для
        корректной работы *NLP* методов использующих (1, 2, 3)-граммы.

        Args:
            df (pd.DataFrame): Датафрейм ключевых слов.

        Returns:
            pd.DataFrame: Датафрейм ключевых слов, с дополнительной колонкой о кол-ве слов в каждом
            ключевом слове.
        """
        result_df = df.copy()
        result_df['Кол-во слов'] = result_df['Ключевые слова'].apply(lambda x: len(x.split()))

        len_keywords_is_3_or_less = result_df['Кол-во слов'] <= 3
        for index, row in result_df.loc[~len_keywords_is_3_or_less].iterrows():
            add_to_err_list_and_logs(exception_info=
                                     {'type': 'invalid_keyword_length',
                                      'word': row['Ключевые слова']},
                                     err_list=self.msg['errors'],
                                     logger=self.logger)
        result_df = result_df.loc[len_keywords_is_3_or_less]
        return result_df

    def fix_keywords(self, df):
        """Метод, преобразующий датафрейм ключевых слов, путем последовательного вызова методов:
        :func:`~check_criteria`, :func:`~check_keywords_lang`, :func:`~check_keyword_length`.

        Args:
            df (pd.DataFrame): Датафрейм ключевых слов.

        Returns:
            pd.DataFrame: Преобразованный датафрейм ключевых слов.
        """
        result_df = df.copy()

        # Приводим все к нижнему регистру и убираем пробелы слева и справа
        result_df = result_df.apply(lambda x: x.astype(str).str.lower())
        result_df = result_df.apply(lambda x: x.astype(str).str.strip())

        # Проверям что-бы не было неизвестных критериев
        result_df = self.check_criteria(result_df)

        # Проверяем что-бы не веса ключевых слов были в допустимом диапозоне
        result_df = self.check_wheight(result_df)

        # Проверяем что-бы не было слов кроме русских и английских и добавляем колонку с языком
        result_df = self.check_keywords_lang(result_df)

        # Определяем кол-во слов в ключевом слове и убирем с кол-вом > 3
        result_df = self.check_keyword_length(result_df)

        result_df = result_df.set_index('Ключевые слова').to_dict(orient='index')

        return result_df
