import io
import traceback

import pandas as pd


class OutputAttHandler:

    def __init__(self, user_request_id, user_logger, requests, organisatins):
        self.user_request_id = user_request_id
        self.logger = user_logger
        self.requests = requests
        self.organisations = organisatins

    def get_domain_info_table(self):
        """Метод, позволяющий по идентификатору запроса пользователя, составить датафрейм,
        содержащий такие колонки, как: **Адрес сайта из запроса**, **Преобразованный адрес**,
        **Перенаправленный адрес**.

        Returns:
            pd.Dataframe: Датафрейм с информацией о сайте организации.
        """
        user_request = self.requests.find_one({'_id': self.user_request_id})
        user_request = user_request if user_request else {}
        site_list = user_request.get('site_list', [])
        df = pd.DataFrame(site_list, columns=['original_url', 'cleaned_url', 'redirected_url', 'domain', 'id', 'name'])
        df = df.rename(columns={'original_url': 'Адрес сайта из запроса',
                                'cleaned_url': 'Преобразованный адрес',
                                'redirected_url': 'Перенаправленный адрес',
                                'domain': 'Сайт',
                                'id': 'ИНН организации',
                                'name': 'Наименование организации'})
        # df.to_csv('Z:\System Data\Рабочий стол\Output\hi.csv')

        return df

    @staticmethod
    def find_domain_info(df, domain):
        """**Статический** метод поиска информации о сайте организации из датафрейма,
        полученного путем вызова метода :func:`~create_redirects_table`.

        Args:
            df (pd.DataFrame): Датафрейм с информацией о сайтах организаций.
            domain (str): Домен, которй следует искать в датафрейме.

        Returns:
            pd.DataFrame: Датафрейм с информацией об интересующих доменах.
        """
        domain_redirect_row = df.loc[df['Сайт'] == domain]

        try:
            domain_redirect_row = domain_redirect_row.iloc[[0]]
            original_url = domain_redirect_row['Адрес сайта из запроса'].item()
            cleaned_url = domain_redirect_row['Преобразованный адрес'].item()
            redirected_url = domain_redirect_row['Перенаправленный адрес'].item()

            domain_redirect_row['Преобразованный адрес'] = 'без изменений' \
                if cleaned_url == original_url or cleaned_url is None else cleaned_url

            domain_redirect_row['Перенаправленный адрес'] = 'без изменений' \
                if redirected_url == cleaned_url or redirected_url is None else redirected_url

        except:
            domain_redirect_row = pd.DataFrame({'Адрес сайта из запроса': domain,
                                                'Преобразованный адрес': 'без изменений',
                                                'Перенаправленный адрес': 'без изменений',
                                                'Сайт': domain,
                                                'ИНН организации': '',
                                                'Наименование организации': ''}, index=[0])

        finally:
            return domain_redirect_row

    def find_domain_about_content(self, domain):
        """Метод поиска описания о сайте организации из базы данных.

        Args:
            domain (str): Домен, которй следует искать в базе данных.

        Returns:
            dict : Словарь с описанием об интересующем домене.
        """

        domain_about_content = self.organisations.find_one({'domain': domain}, {'_id': 0, 'about_content': 1})
        return domain_about_content

    def create_ouput_att(self, domain_rel_list):
        """Метод, составляющий результирующий **excel** файл на основе информации полученной
        путем вызова предыдущих методов.

        Args:
            domain_rel_list (list): Список полученный путем вызова метода
                :func:`~find_relevance_tables`.

        Returns:
            bytearray: Набор байт, соответствующий **excel** файлу, содержащему результат обработки
            запроса пользователя.
        """
        self.logger.add_log('Начинается сборка результата в excel формат')

        try:
            output = io.BytesIO()
            writer = pd.ExcelWriter(output, engine='xlsxwriter')

            workbook = writer.book

            formats = self.get_file_formats(workbook=workbook)
            ws_format = formats['ws_format']
            header_format = formats['header_format']
            main_header_format = formats['main_header_format']
            merge_about_format = formats['merge_about_format']

            summary_worksheet = workbook.add_worksheet('Запрос 2.1')
            detailed_worksheet = workbook.add_worksheet('Запрос 2.2')
            detailed_worksheet_request = workbook.add_worksheet('Запрос 3')
            writer.sheets['Запрос 2.1'] = summary_worksheet
            writer.sheets['Запрос 2.2'] = detailed_worksheet
            writer.sheets['Запрос 3'] = detailed_worksheet_request

            self.fill_summary_att_sheet(writer=writer,
                                        worksheet=summary_worksheet,
                                        domain_rel_list=domain_rel_list,
                                        ws_format=ws_format,
                                        header_format=header_format,
                                        main_header_format=main_header_format)

            self.fill_detailed_att_sheet(writer=writer,
                                         worksheet=detailed_worksheet,
                                         domain_rel_list=domain_rel_list,
                                         ws_format=ws_format,
                                         header_format=header_format,
                                         merge_about_format=merge_about_format,
                                         main_header_format=main_header_format)

            self.fill_detailed_att_sheet_req(writer=writer,
                                             worksheet=detailed_worksheet_request,
                                             domain_rel_list=domain_rel_list,
                                             ws_format=ws_format,
                                             header_format=header_format,
                                             merge_about_format=merge_about_format,
                                             main_header_format=main_header_format)

            writer.save()
            xlsx_data = output.getvalue()
            self.logger.add_log('Успешно создано вложение с результатами.')

            return xlsx_data

        except Exception as err:
            self.logger.add_log(
                'Ошибка при создании вложения с результатами: {}. Детали: {}.'.format(
                    repr(err), ''.join(traceback.format_tb(err.__traceback__))
                )
            )

    @staticmethod
    def get_file_formats(workbook):
        link_format = workbook.get_default_url_format()
        link_format.set_align('center')
        link_format.set_valign('vcenter')

        ws_format = workbook.add_format({'align': 'center', 'valign': 'vcenter'})
        header_format = workbook.add_format({
            'bold': True,
            'text_wrap': True,
            'fg_color': '#305496',
            'border': 1,
            'align': 'center',
            'valign': 'center',
            'font_color': 'white'})

        main_header_format = workbook.add_format({
            'bold': True,
            'text_wrap': True,
            'bg_color': '#305496',
            'align': 'left',
            'font_size': '16',
            'valign': 'left',
            'font_color': 'white'})

        merge_about_format = workbook.add_format({'align': 'center', 'valign': 'top', 'text_wrap': True})

        formats = {'ws_format': ws_format,
                   'header_format': header_format,
                   'main_header_format': main_header_format,
                   'merge_about_format': merge_about_format}

        return formats

    def fill_summary_att_sheet(self, worksheet, writer, domain_rel_list, ws_format, header_format, main_header_format):
        worksheet.merge_range('A1:E1', 'Сводный результат оценки релевантности сайтов по ключевым словам',
                              main_header_format)
        row = 2
        max_widths_list = [25, 25, 20, 30, 20]
        worksheet.set_row(0, 25, ws_format)

        relevence_sum_col = 4

        domain_info_df = self.get_domain_info_table()
        headers = ['Адрес сайта из запроса', 'Преобразованный адрес', 'ИНН организации', 'Наименование организации',
                   'Релевантность сайта']
        for col_num, header in enumerate(headers):
            worksheet.write(row, col_num, header, header_format)

        for domain in domain_rel_list:
            domain_info = self.find_domain_info(domain_info_df, domain['domain'])
            domain_info = domain_info[
                ['Адрес сайта из запроса', 'Преобразованный адрес', 'ИНН организации', 'Наименование организации']]
            for i, width in enumerate(max_widths_list):
                worksheet.set_column(i, i, width, ws_format)

            domain_info.to_excel(writer, sheet_name=worksheet.name,
                                 startrow=row + 1, startcol=0, index=False, header=False)

            # worksheet.write(row + 1, relevence_sum_col, "{:.2f}".format(domain['relevance_sum']))
            worksheet.write(row + 1, relevence_sum_col, int(domain['relevance_sum']))

            row = row + 1
        return worksheet

    def fill_detailed_att_sheet(self, worksheet, writer, domain_rel_list, ws_format, header_format, merge_about_format,
                                main_header_format):
        worksheet.merge_range('A1:E1', 'Детальный результат оценки релевантности сайтов по ключевым словам',
                              main_header_format)
        row = 2
        spaces = 1
        max_widths_list = [25, 25, 25, 30, 30, 20,
                           20, 20, 20]
        worksheet.set_row(0, 25, ws_format)
        relevence_sum_col = 8
        start_relevance_col = 2

        domain_info_df = self.get_domain_info_table()

        for domain in domain_rel_list:
            relevance_df = domain['relevance_by_word'][['Общее кол-во слов',
                                                        'Среднее кол-во вхождения слов',
                                                        'Первый квартиль вхождения слов',
                                                        'Медиана вхождения слов',
                                                        'Третий квартиль вхождения слов',
                                                        'Ключевое слово',
                                                        'Кол-во вхождений ключевого слова',
                                                        'Критерий поиска ключевого слова',
                                                        'Приведенная частота вхождения ключевого слова',
                                                        'Вес ключевого слова',
                                                        'Приведенная частота вхождения ключевого слова с учетом веса',
                                                        'Кол-во вхождений ключевого слова с учетом веса']]

            domain_info = self.find_domain_info(domain_info_df, domain['domain'])
            # domain_about_dict = self.find_domain_about_content(domain['domain'])
            # domain_about_df = pd.DataFrame.from_dict(domain_about_dict, orient='index')
            # domain_about_df = domain_about_df.rename(columns={'about_page': 'Адрес сайта описания организации',
            #                                                   'about_text': 'Описание организации'})

            for i, width in enumerate(max_widths_list):
                worksheet.set_column(i, i, width, ws_format)

            headers = ['Адрес сайта из запроса', 'Преобразованный адрес'] + ['Общее кол-во слов',

                                                                             'Ключевое слово',
                                                                             'Критерий поиска ключевого слова',
                                                                             'Вес ключевого слова',
                                                                             'Кол-во найденных ключевых слов',
                                                                             'Оценка по ключевому слову'] + [
                          'Релевантность сайта']

            for col_num, header in enumerate(headers):
                worksheet.write(row, col_num, header, header_format)
                worksheet.set_row(row, 30, ws_format)
            for col_num, __ in enumerate(['Адрес сайта из запроса', 'Преобразованный адрес']):
                worksheet.merge_range(first_row=row + 1, first_col=col_num,
                                      last_row=row + len(relevance_df.index), last_col=col_num,
                                      data='', cell_format=ws_format)

            domain_info = domain_info[['Адрес сайта из запроса', 'Преобразованный адрес']]
            domain_info.to_excel(writer, sheet_name=worksheet.name,
                                 startrow=row + 1, startcol=0, index=False, header=False)

            worksheet.merge_range(first_row=row + 1, first_col=relevence_sum_col,
                                  last_row=row + len(relevance_df.index), last_col=relevence_sum_col,
                                  data='', cell_format=ws_format)

            # worksheet.write(row + 1, relevence_sum_col, "{:.2f}".format(domain['relevance_sum']))
            worksheet.write(row + 1, relevence_sum_col, int(domain['relevance_sum']))
            relevance_df_tp = relevance_df[['Общее кол-во слов',
                                            'Ключевое слово',
                                            'Критерий поиска ключевого слова',
                                            'Вес ключевого слова',
                                            'Кол-во вхождений ключевого слова']]
            relevance_df_tp['Оценка по ключевому слову'] = relevance_df_tp['Кол-во вхождений ключевого слова'] * \
                                                           relevance_df_tp['Вес ключевого слова']
            # relevance_df_tp.to_csv('Z:\System Data\Рабочий стол\Output\\t.csv')
            relevance_df = self.prepare_relevance_df_to_excel(relevance_df_tp)
            # Записываем styler датафрейм в excel
            relevance_df.to_excel(writer, sheet_name=worksheet.name,
                                  startrow=row + 1, startcol=start_relevance_col, index=False, header=False)

            # self.merge_columns(worksheet, row, domain_info, relevance_df.data, ws_format)

            # for index, col in enumerate(domain_about_df.columns):
            #     if col == 'Адрес сайта описания организации':
            #         worksheet.merge_range(first_row=row + 1, first_col=start_domain_about_col + index,
            #                               last_row=row + len(relevance_df.index),
            #                               last_col=start_domain_about_col + index,
            #                               data='',
            #                               cell_format=ws_format)
            #         domain_about_df[[col]].to_excel(writer, sheet_name=worksheet.name,
            #                                         startrow=row + 1, startcol=start_domain_about_col,
            #                                         index=False, header=False)
            #     elif col == 'Описание организации':
            #         worksheet.merge_range(first_row=row + 1, first_col=start_domain_about_col + index,
            #                               last_row=row + len(relevance_df.index),
            #                               last_col=start_domain_about_col + index,
            #                               data=domain_about_df[col]['about_content'],
            #                               cell_format=merge_about_format)

            row = row + len(relevance_df.index) + spaces + 1

    def fill_detailed_att_sheet_req(self, worksheet, writer, domain_rel_list, ws_format, header_format,
                                    merge_about_format,
                                    main_header_format):
        worksheet.merge_range('A1:E1', 'Детальный результат оценки релевантности сайтов по ключевым словам',
                              main_header_format)
        row = 2
        spaces = 1
        max_widths_list = [25, 25, 25, 20, 20, 30,
         20, 20, 33, 35, 30, 35,
         25, 35, 32, 47, 20, 60,
         50, 40, 70]

        worksheet.set_row(0, 25, ws_format)
        worksheet.set_row(2, 35, ws_format)
        start_domain_about_col = 4
        relevence_sum_col = 3
        start_relevance_col = 7

        domain_info_df = self.get_domain_info_table()

        for domain in domain_rel_list:
            relevance_df = domain['relevance_by_word'][['Общее кол-во слов',
                                                        'Среднее кол-во вхождения слов',
                                                        'Первый квартиль вхождения слов',
                                                        'Медиана вхождения слов',
                                                        'Третий квартиль вхождения слов',
                                                        'Ключевое слово',
                                                        'Кол-во вхождений ключевого слова',
                                                        'Критерий поиска ключевого слова',
                                                        'Приведенная частота вхождения ключевого слова',
                                                        'Вес ключевого слова',
                                                        'Приведенная частота вхождения ключевого слова с учетом веса',
                                                        'Кол-во вхождений ключевого слова с учетом веса']]

            domain_info = self.find_domain_info(domain_info_df, domain['domain'])
            domain_about_dict = self.find_domain_about_content(domain['domain'])
            domain_about_df = pd.DataFrame.from_dict(domain_about_dict, orient='index')
            domain_about_df = domain_about_df.rename(columns={'about_page': 'Адрес сайта описания организации',
                                                              'about_text': 'Описание организации'})

            for i, width in enumerate(max_widths_list):
                worksheet.set_column(i, i, width, ws_format)

            headers = ['Адрес сайта из запроса', 'Преобразованный адрес', 'ИНН организации', 'Наименование организации'] + list(domain_about_df.columns)

            for col_num, header in enumerate(headers):
                worksheet.write(row, col_num, header, header_format)
            for col_num, __ in enumerate(['Адрес сайта из запроса', 'Преобразованный адрес', 'ИНН организации', 'Наименование организации']):
                worksheet.merge_range(first_row=row + 1, first_col=col_num,
                                          last_row=row + len(relevance_df.index), last_col=col_num,
                                          data='', cell_format=ws_format)
            domain_info = domain_info[['Адрес сайта из запроса', 'Преобразованный адрес', 'ИНН организации', 'Наименование организации']]
            domain_info.to_excel(writer, sheet_name=worksheet.name,
                                 startrow=row + 1, startcol=0, index=False, header=False)

            # worksheet.merge_range(first_row=row + 1, first_col=relevence_sum_col,
            #                       last_row=row + len(relevance_df.index), last_col=relevence_sum_col,
            #                       data='', cell_format=ws_format)

            # worksheet.write(row + 1, relevence_sum_col, "{:.2f}".format(domain['relevance_sum']))
            # worksheet.write(row + 1, relevence_sum_col, int(domain['relevance_sum']))

            relevance_df = self.prepare_relevance_df_to_excel(relevance_df)

            # Записываем styler датафрейм в excel
            # relevance_df.to_excel(writer, sheet_name=worksheet.name,
            #                       startrow=row + 1, startcol=start_relevance_col, index=False, header=False)

            # self.merge_columns(worksheet, row, domain_info, relevance_df.data, ws_format)

            for index, col in enumerate(domain_about_df.columns):
                if col == 'Адрес сайта описания организации':
                    worksheet.merge_range(first_row=row + 1, first_col=start_domain_about_col + index,
                                          last_row=row + len(relevance_df.index),
                                          last_col=start_domain_about_col + index,
                                          data='',
                                          cell_format=ws_format)
                    domain_about_df[[col]].to_excel(writer, sheet_name=worksheet.name,
                                                    startrow=row + 1, startcol=start_domain_about_col,
                                                    index=False, header=False)
                elif col == 'Описание организации':
                    worksheet.merge_range(first_row=row + 1, first_col=start_domain_about_col + index,
                                          last_row=row + len(relevance_df.index),
                                          last_col=start_domain_about_col + index,
                                          data=domain_about_df[col]['about_content'],
                                          cell_format=merge_about_format)

            row = row + len(relevance_df.index) + spaces + 1

    @staticmethod
    def merge_columns(worksheet, row, domain_info_dataframe, relevance_dataframe, merge_format):

        statistic_cols = ['Общее кол-во слов']

        domain_info_cols_to_merge = list(range(0, len(domain_info_dataframe.columns)))
        relevance_df_cols_to_merge = [relevance_dataframe.columns.get_loc(col) + len(domain_info_cols_to_merge) + 1
                                      for col in statistic_cols]
        columns_to_merge = domain_info_cols_to_merge + relevance_df_cols_to_merge

        merge_values = list(domain_info_dataframe.iloc[0]) + list(relevance_dataframe[statistic_cols].iloc[0])

        for index, col_index in enumerate(columns_to_merge):
            worksheet.merge_range(first_row=row + 1, first_col=col_index,
                                  last_row=row + len(relevance_dataframe.index),
                                  data=merge_values[index],
                                  last_col=col_index, cell_format=merge_format)

    def prepare_relevance_df_to_excel(self, df):
        result_df = df.copy()

        # Переименование критериев
        result_df['Критерий поиска ключевого слова'] = \
            result_df['Критерий поиска ключевого слова'].apply(lambda x: self.rename_criteria(x))

        # Оставляем нужное кол-во знаков после запятой у float
        for col in result_df.loc[:, result_df.dtypes == float]:
            result_df[col] = result_df[col].astype(float).round(2)

        # Применяем цвет к ключевым словам
        result_df = result_df.style.apply(self.highlight_greaterthan, column='Критерий поиска ключевого слова', axis=1)

        # Применяем выравнивание к styler датафрейму
        result_df = result_df.set_properties(**{'text-align': 'center', 'vertical-align': 'middle'})

        return result_df

    @staticmethod
    def highlight_greaterthan(data, column):
        return ['color: red' if data['Критерий поиска ключевого слова'] == 'стоп-слово' else '' for v in data]

    @staticmethod
    def rename_criteria(val):
        criteria = val
        if val == 'v':
            criteria = 'стемминг и лемматизация'
        elif val == 'n':
            criteria = 'без преобразований'
        elif val == 's':
            criteria = 'стоп-слово'
        return criteria
