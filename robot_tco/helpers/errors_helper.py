from settings import CRITERIA_RANGE, N_GRAM_MAX, WEIGHT_RANGE


def add_to_err_list_and_logs(exception_info, err_list, logger):
    """Функция, отвечающая за добаление сообщения об ошибке как в список логгера, так и в список
    ошибок конкретного сообщения.

    Args:
        exception_info (dict): Словарь, которые содержит тип ошибки, а также необходимую информуцию,
            которую нужно сообщить пользовать и залогировать.
        err_list (list): Список ошибок сообщения.
        logger (:class:`~logs.logs_helper.Logger`): Логгер данного сообщения.
    """
    err_msg = ''
    log_msg = ''

    if exception_info['type'] == 'url_unicode':
        err_msg = 'Ошибка подключения к сайту "{}": {}.'. \
            format(exception_info['site_url'], 'Формат URL не удовлетворяет требованиям.')
        log_msg = err_msg

    elif exception_info['type'] == 'url_connection':
        if exception_info['original_org_url'] != exception_info['cleaned_org_url']:
            err_msg = 'Ошибка подключения к "{}", который был преобразован к "{}".' \
                .format(exception_info['original_org_url'], exception_info['cleaned_org_url'])
            log_msg = 'Ошибка подключения к "{}",' \
                      ' который был преобразован к "{}": {}. Детали: {}' \
                .format(exception_info['original_org_url'],
                        exception_info['cleaned_org_url'],
                        exception_info['repr_err'],
                        exception_info['err_traceback'])
        else:
            err_msg = 'Ошибка подключения к "{}"' \
                .format(exception_info['original_org_url'], exception_info['cleaned_org_url'])
            log_msg = 'Ошибка подключения к "{}": {}. Детали: {}' \
                .format(exception_info['original_org_url'],
                        exception_info['cleaned_org_url'],
                        exception_info['repr_err'],
                        exception_info['err_traceback'])

    elif exception_info['type'] == 'url_unknown':
        if exception_info['original_org_url'] != exception_info['cleaned_org_url']:
            err_msg = 'Непредвиденная ошибка подключения к "{}", который был преобразован к "{}".' \
                .format(exception_info['original_org_url'], exception_info['cleaned_org_url'])
            log_msg = 'Непредвиденная ошибка подключения к "{}",' \
                      ' который был преобразован к "{}": {}. Детали: {}' \
                .format(exception_info['original_org_url'],
                        exception_info['cleaned_org_url'],
                        exception_info['repr_err'],
                        exception_info['err_traceback'])
        else:
            err_msg = 'Непредвиденная ошибка подключения к "{}"' \
                .format(exception_info['original_org_url'], exception_info['cleaned_org_url'])
            log_msg = 'Непредвиденная ошибка подключения к "{}": {}. Детали: {}' \
                .format(exception_info['original_org_url'],
                        exception_info['cleaned_org_url'],
                        exception_info['repr_err'],
                        exception_info['err_traceback'])

    elif exception_info['type'] == 'sites number':
        err_msg = exception_info['err']
        log_msg = err_msg

    elif exception_info['type'] == 'att_xlrd_err':
        err_msg = 'Ошибка при чтении вложенного файла. ' \
                  'Убедитесь, что он соответствует шаблону. ' \
                  'Или, что он не пустой.'
        log_msg = 'Ошибка при чтении вложенного файла: {}. Детали: {}' \
            .format(exception_info['repr_err'], exception_info['err_traceback'])

    elif exception_info['type'] == 'att_validation_err':
        err_msg = 'Ошибка валидации вложенного файла: {}. Убедитесь, что он соответствует шаблону.' \
            .format(exception_info['err_msg'])
        log_msg = 'Ошибка валидации вложенного файла: {}. Детали: {}' \
            .format(exception_info['repr_err'], exception_info['err_traceback'])

    elif exception_info['type'] == 'att_common_err':
        err_msg = 'Ошибка при обработке вложенного файла.'
        log_msg = 'Ошибка при обработке вложенного файла: {}. Детали: {}' \
            .format(exception_info['repr_err'], exception_info['err_traceback'])

    elif exception_info['type'] == 'keyword_criteria_err':
        err_msg = 'Критерий "{}" ключевого слова "{}" не соответствует ни одному из доступных: {}' \
            .format(exception_info['criteria'], exception_info['word'], str(CRITERIA_RANGE))
        log_msg = err_msg

    elif exception_info['type'] == 'weight_range_err':
        err_msg = 'Вес "{}" ключевого слова "{}" не входит в диапозон: от {} до {}. А также не равен значению {}, ' \
                  'которое соответствует занулению релевантности ресурса (применительно только для стоп-слов).' \
            .format(exception_info['weight'], exception_info['word'],
                    str(WEIGHT_RANGE['min_weight']), str(WEIGHT_RANGE['max_weight']),
                    str(WEIGHT_RANGE['zeroing_out_weight']))
        log_msg = err_msg

    elif exception_info['type'] == 'invalid_keyword_lang':
        err_msg = 'Ключевое слово "{}" не на rus/eng языке или содержит недопустимые символы' \
            .format(exception_info['word'])
        log_msg = err_msg

    elif exception_info['type'] == 'invalid_keyword_length':
        err_msg = 'Ключевое слово "{}" состоит из более чем {} слов' \
            .format(exception_info['word'], N_GRAM_MAX)
        log_msg = err_msg

    elif exception_info['type'] == 'duplicated_org':
        err_msg = 'Сайт организации "{}" входит в набор больше чем 1 раз.' \
                  ' Использоваться будет только первое вхождение этого сайта.' \
            .format(exception_info['dup_item'], N_GRAM_MAX)
        log_msg = err_msg

    elif exception_info['type'] == 'duplicated_keyword':
        err_msg = 'Ключевое слово "{}" входит в набор больше чем 1 раз.' \
                  ' Использоваться будет только первое вхождение этого слова.' \
            .format(exception_info['dup_item'], N_GRAM_MAX)
        log_msg = err_msg

    elif exception_info['type'] == 'redirected_url':
        err_msg = 'Сайт организации "{}" был перенаправлен на сайт "{}"' \
            .format(exception_info['original_url'], exception_info['redirect_url'])
        log_msg = err_msg

    elif exception_info['type'] == 'no_sites_connected':
        err_msg = 'Не удалось подключиться ни к одному сайту. Работа программы остановлена.'
        log_msg = err_msg

    elif exception_info['type'] == 'scraping_err':
        err_msg = 'Ошибка при скаппинге сайта "{}".'.format(exception_info['domain'])
        log_msg = 'Ошибка при скаппинге сайта "{}" {}. Детали: {}' \
            .format(exception_info['domain'],
                    exception_info['repr_err'],
                    exception_info['err_traceback'])

    elif exception_info['type'] == 'process_data':
        err_msg = 'Непредвиденная ошибка при обработке текстового содержимого сайтов'
        log_msg = 'Непредвиденная ошибка при обработке текстового содержимого сайтов: {}.' \
                  ' Детали: {}'.format(exception_info['repr_err'],
                                       exception_info['err_traceback'])

    if err_msg and log_msg:
        err_list.append(err_msg)
        logger.add_log(log_msg)
