import re
import ssl
import urllib.error
from urllib.request import urlopen
from urllib.parse import urlparse, urlunparse, urlunsplit, urljoin, ParseResult, urlsplit


def url_cleaner(url):
    """Функция предназначена приведения URL сайта к нормальному виду. Добавление к URL из запроса
    название протокола подключения, а также домена, если необходимо. Также применяется кодировка
    *idna*, которая приводит русскоязычные адреса к формату IDN. Декодировка происходит с помощью
    функции :func:`~url_deocde`.

    Args:
        url (str): Исходные адрес сайта организации.

    Returns:
        str: Приведенный к стандартному виду адрес сайта организации.
    """
    site_url = url.strip()
    # Слеш в конце лишний
    if site_url.endswith('/'):
        site_url = site_url[:-1]

    # Убираем ненужный домен www для устранения дубликатов
    if 'www.' in site_url:
        site_url = site_url.split('www.')
        site_url = ''.join(site_url)

    site_url_parsed = urlparse(site_url)

    # Преобразование URL к нормальному виду
    if not site_url_parsed.scheme:
        site_url = 'http://' + site_url_parsed.path.encode('idna').decode('utf-8')
    else:
        site_url = site_url_parsed.scheme + '://' + site_url_parsed.netloc.encode('idna').decode('utf-8')

    return site_url


def get_domain(url):
    """Функция, позволяющая получить значение домена второго уровня сайта организации.

    Args:
        url (str): Сайт организации.

    Returns:
        str: Домен сайта организации.
    """
    domain = urlparse(url).netloc
    return domain


def open_url(url, get_html=False):
    """Функция, которая пытается открыть сайт организации вне зависимости от наличия у этого
    сайта сертификата SSL. При переадресации, запоминается адрес, на который произошло
    перенаправление.

    Args:
        url (str): Сайт организации

    Returns:
        str: Конечный сайт организации, если удалость подключиться, если нет, то - **None**
    """
    # Выключаем проверку сертификата
    ctx = ssl.create_default_context()
    ctx.check_hostname = False
    ctx.verify_mode = ssl.CERT_NONE

    # Пробуем открыть URL
    try:
        res = urlopen(url, context=ctx)
    except (urllib.error.URLError, ConnectionResetError):
        # Возможно не https - неверный протокол для этого сайта, меняем на http
        new_url = 'http://' + url.split('//')[1]
        res = urlopen(new_url, context=ctx)

    if get_html:
        return res.read()
    else:
        final_url = res.geturl()
        return final_url


def define_lang(url):
    """Функция, определяющая язык, на котором написан URL. Возможна идентификация только *ru/en*,
    языка.

    Args:
        url (str): Сайт организации.

    Returns:
        str: Язык *(ru/en/unknown)* URL данного сайта.
    """
    match = re.match('^[:?=&/0-9a-zA-Zа-яА-ЯёЁ_\-.]+$', url)
    if match:
        match_en = re.match('^[:?=&/0-9a-zA-Z_\-.]+$', url)
        if match_en:
            return 'en'
        else:
            return 'rus'
    else:
        return 'wrong url'


def url_decode(url):
    """Функция, декодирующая URL из формата IDN.

    Args:
        url (str): Сайт или домен организации.

    Returns:
        str: Декодированный сайт или домен организации.
    """
    url_first_part = ''
    url_main_part = url
    if '//' in url:
        url_first_part = url.split('//')[0] + '//'
        url_main_part = url.split('//')[1]
    if url_main_part[-1] == '/':
        url_main_part = url_main_part[:-1]

    decoded_url_main_path = []
    try:
        decoded_url_main_path = url_main_part.encode('utf-8').decode('idna')
    except UnicodeError:
        for sub_path in url_main_part.split('/'):
            try:
                decoded_url_main_path.append(sub_path.encode('utf-8').decode('idna'))
            except UnicodeError:
                decoded_url_main_path.append(sub_path)
        decoded_url_main_path = '/'.join(decoded_url_main_path)

    decoded_url = url_first_part + decoded_url_main_path
    return decoded_url


def check_redirect(url):
    """Проверяет совпадение конечного адреса сайта с тем, к которому пытались подключиться.

    Args:
        url (str): Сайт организации.

    Returns:
        str: Конечный сайт организации.
    """
    org_url = url_cleaner(url)
    redirected_org_url = open_url(url=org_url)

    # Декодируем URL
    org_url = url_decode(url)
    redirected_org_url = url_decode(redirected_org_url)

    if org_url != redirected_org_url:
        return redirected_org_url
