import urllib
from pprint import pprint
from urllib.error import URLError
from urllib.parse import urljoin

import pandas as pd

import html5lib
from bs4 import BeautifulSoup

from helpers.url_helper import url_cleaner, open_url, url_decode


def get_site_urls(html_text, site_url):
    soup = BeautifulSoup(html_text, 'html5lib')
    about_hrefs = []
    for link in soup.find_all('a'):
        link_href = link.get('href', '')
        link_details = {'link_url': urljoin(site_url, link.get('href')),
                        'href': link_href.lower().strip().replace('\r', '').replace('\n', ''),
                        'link_text': link.text.lower().strip().replace('\r', '').replace('\n', '')}
        about_hrefs.append(link_details) if link_details not in about_hrefs else None
    return about_hrefs


def get_about_page(site_urls, index_page_url):
    about_pages = []

    about_text_patterns = ["о компании", "компания", "описание деятельности", "деятельности", "деятельность", "о нас"]
    about_href_patterns = ['about', 'o-kompanii', 'o-nas', 'о-компании', 'о-нас']

    for url in site_urls:
        for pattern in about_href_patterns:
            if url['href'] and url['href'] in pattern or pattern in url['href']:
                about_pages.append(url['link_url']) if url['link_url'] not in about_pages else None
                continue
        for pattern in about_text_patterns:
            if url['link_text'] and url['link_text'] in pattern or pattern in url['link_text']:
                about_pages.append(url['link_url']) if url['link_url'] not in about_pages else None
                continue
    if about_pages:
        return about_pages[0]
    else:
        return index_page_url


def get_text_from_about_page(html_text):
    tree = BeautifulSoup(html_text, 'html5lib')
    body = tree.body

    if body is None:
        return ''

    for selection in ['style', 'script', 'head', 'title', 'meta', '[document]']:
        for tag in body.select(selection):
            tag.extract()

    sep = '!#-#-#!'
    text = body.get_text(separator=sep)
    text_list = text.split(sep)

    cleaned_text = []
    for text_item in text_list:
        item = text_item.strip().replace('  ', ' ')
        if len(item.split()) > 10:
            cleaned_text.append(item)
    cleaned_text = '\n'.join(cleaned_text)

    cleaned_text = cleaned_text[:2045] + '...'

    return cleaned_text


def get_about_text(site_url):
    result = {}
    about_page = None
    try:
        cleaned_site_url = url_cleaner(site_url)
        site_html = open_url(cleaned_site_url, get_html=True)
        urls = get_site_urls(site_html, cleaned_site_url)
        about_page = get_about_page(urls, cleaned_site_url)
    except (UnicodeEncodeError, TypeError, urllib.error.URLError, ConnectionResetError):
        pass

    if about_page:
        about_page = url_decode(about_page)
        result['about_page'] = about_page
        try:
            resp = open_url(about_page, get_html=True)
        except (UnicodeEncodeError, TypeError, urllib.error.URLError, ConnectionResetError):
            resp = ''

        about_text = get_text_from_about_page(resp)
        if about_text:
            result['about_text'] = about_text
            return result
        else:
            result['about_text'] = 'Не удалось найти описание, либо не удалось зайти на страницу с описанием'
            return result
    else:
        result['about_page'] = 'Не удалось найти страницу с описанием'
        result['about_text'] = ''
        return result
