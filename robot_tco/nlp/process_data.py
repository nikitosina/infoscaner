import io
import itertools
import os
import re
import traceback
import pathlib
from collections import Counter
from pprint import pprint

import nltk
import pandas as pd
import pymorphy2
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from nltk.stem.snowball import SnowballStemmer
from sklearn.feature_extraction.text import CountVectorizer


class SiteTextProcessor:
    """Класс :class:`~SiteTextProcessor` предназначен для работы с заранее собранными текстовыми
    содержимыми различных сайтов. Работа данного класса предполагает сбор и анализ статистических
    данных о тексте, а также присвоение релевантности из сравнения ресурсов.

    Attributes:
        user_request_id (bson.ObjectId): Идентификатор документа в базе данных, соответствующего
            конкретному пользовательскому запросу.
        requests (MongoDBCollection): Коллекция документов пользовательских обращений в базе данных.
        organisations (MongoDBCollection): Коллекция документов организаций в базе данных.
        user_logger (~logs.logs_helper.Logger): Логгер данного пользовательского обращения.
        stopwords (list): Список стоп-слов. Задается функцией :func:`~set_stop_words`.
        ru_stemmer (nltk.SnowballStemmer): Стеммер для русского языка.
        en_stemmer (nltk.SnowballStemmer): Стеммер для английского языка.
        ru_lemmatizer (pymorphy2.MorphAnalyzer): Лемматизатор для русского языка.
        en_lemmatizer (nltk.WordNetLemmatizer): Лемматизатор для английского языка.

    Args:
        user_request_id (bson.ObjectId): Идентификатор документа в базе данных, соответствующего
            конкретному пользовательскому запросу.
        requests (MongoDBCollection): Коллекция документов пользовательских обращений в базе данных.
        organisations (MongoDBCollection): Коллекция документов организаций в базе данных.
        user_logger (~logs.logs_helper.Logger): Логгер данного пользовательского обращения.
    """

    def __init__(self, user_request_id, requests, organisations, user_logger):

        self.user_request_id = user_request_id
        self.requests = requests
        self.organisations = organisations
        self.stopwords = []

        self.logger = user_logger

        self.set_nltk_path()
        self.set_stop_words()

        self.ru_stemmer = SnowballStemmer("russian")
        self.en_stemmer = SnowballStemmer("english")

        self.ru_lemmatizer = pymorphy2.MorphAnalyzer()
        self.en_lemmatizer = WordNetLemmatizer()

    def set_nltk_path(self):
        """Метод добавляющий путь к вспомогательной информации библиотеки ``nltk``.
        Необходим для дальнейшей загрузки стоп-слов
        """
        try:
            nltk.data.path = [os.path.join(os.path.abspath('robot_tco/nlp/nltk_data'),
                                           pathlib.Path(__file__).parent.absolute(),
                                           'nltk_data')]
        except:
            self.logger.add_log('Ошибка при поиске папки nltk_data')

    def set_stop_words(self):
        """Метод, позволяющий добавить русские и английские стоп-слова в список стоп-слов данного
        класса.
        """
        try:
            self.stopwords = stopwords.words('russian') + stopwords.words('english')
        except LookupError:
            self.logger.add_log('Не удалось найти стоп слова')
            self.stopwords = []

    def remove_stop_words(self, text):
        """Метод, удаляющий стоп-слова из теста.

        Args:
            text (str): Текстовые данные.

        Returns:
            str: Очищенные текстовые данные.
        """
        tokens = text.split(' ')
        cleaned_tokens = []
        for token in tokens:
            if token not in self.stopwords and len(token) > 1:
                cleaned_tokens.append(token)
        result_text = ' '.join(cleaned_tokens)
        return result_text

    def clear_text(self, txt):
        """Метод, преобразующий текст к стандартному для обработки виду, приведения слов к нижнему
        регистру, удаления спец. символов и вызова метода :func:`~remove_stop_words`.

        Args:
            txt (str): Текстовые данные.

        Returns:
            str: Преобразованные текстовые данные.
        """
        result_txt = txt.lower()
        result_txt = re.sub(r'(?!(\b[\w\-]+\b))\W+', ' ', result_txt)
        result_txt = self.remove_stop_words(result_txt)
        return result_txt

    def get_docs(self, mongo_document):
        """Метод, позволяющий получить список всех текстов сайтов из базы данных
        в пределах одного домена.

        Args:
            mongo_document (dict): Словарь, соответствующий документу организации, в нем хранятся
                текстовая информация всех документов по данному домену.

        Returns:
            list: Список текстов.
        """
        pages = mongo_document['pages_texts']
        docs = [self.clear_text(page['text']) for page in pages]
        return docs

    def get_unique_pages(self, mongo_document):
        """Метод, позволяющий удалить дубликаты текстов разных страниц сайта организации, после
        вызова метода :func:`~get_docs`.

        Args:
            mongo_document (dict): Словарь, соответствующий документу организации, в нем хранятся
                текстовая информация всех документов по данному домену.

        Returns:
            list: Список уникальных текстов.
        """
        pages = self.get_docs(mongo_document)
        unique_pages = list(set(pages))
        return unique_pages

    def split_documents_by_words(self, mongo_document):
        pages = self.get_unique_pages(mongo_document)
        pages_texts = [page.split(' ') for page in pages]
        return pages_texts

    def get_full_text(self, mongo_document):
        """Метод, с помошью которого можно получить склеенных текст из списка текстов свех страниц
        сайта, путем вызова :func:`~get_unique_pages`.

        Args:
            mongo_document (dict): Словарь, соответствующий документу организации, в нем хранятся
                текстовая информация всех документов по данному домену.

        Returns:
            str: Склеенный текст страниц сайта.
        """
        pages = self.get_unique_pages(mongo_document)
        full_text = ' '.join(pages)
        return full_text

    @staticmethod
    def create_counters(pages_as_docs):
        counters = []
        for page in pages_as_docs:
            counter = Counter(page)
            counters.append(counter)
        return counters

    @staticmethod
    def create_full_site_df(full_site_text):
        c = Counter(full_site_text.split(' '))
        df = pd.DataFrame(data=c.values(), index=c.keys(), columns=['count'])
        return df

    @staticmethod
    def count_word_document_freq(word, counters):
        how_much_appeared = 0
        for counter in counters:
            if word in counter.keys():
                how_much_appeared += 1
        return how_much_appeared

    def stemmed_and_lemmatized(self, word, lang):
        """Метод, который по слову проводит стемминг, и лемматизацию.

        Args:
            word (str): Слово по которому необходимо провести стеммаинг и лемматизацию.
            lang (str): Язык *ru/en* слова.

        Returns:
            list: Список, состоящих из оригинального слово, слова с пирмененным стеммингом,
            а также всех слов, полученных в ходе лемматизации.
        """
        result_words = [word]
        lemmas = []

        if lang == 'ru':
            result_words.append(self.ru_stemmer.stem(word))
            lemmas = [lemma.normal_form for lemma in self.ru_lemmatizer.parse(word)]

        elif lang == 'en':
            result_words.append(self.en_stemmer.stem(word))
            lemmas = [self.en_lemmatizer.lemmatize(word)]

        result_words.extend(list(set(lemmas)))

        return list(set(result_words))

    def find_word_combinations(self, word):
        """Метод, составляющий кобинации слов, содержащихся в ключевом слове, после последовательно
        примененной к ним функции :func:`~stemmed_and_lemmatized`. Это позволяет улучшить поиск по
        данному ключевому слову.

        Args:
            word (str): Ключевое слово, состоящее из 1-3 слов.

        Returns:
            list: Комбинации преобразованных слов ключевого слова.
        """
        if word['Критерий поиска ключевого слова'] in ['v', 's']:
            lang = word['Язык ключевого слова']
            stemmed_key_word = []

            for word in word.name.split():
                stemmed = self.stemmed_and_lemmatized(word, lang)
                stemmed_key_word.append(stemmed)
            stemmed_key_word = list(map(' '.join, itertools.product(*stemmed_key_word)))
        else:
            stemmed_key_word = [word.name]

        return stemmed_key_word

    def get_scraped_sites(self):
        """Метод, с помощью которого можно получить список доменов, к которым был успешно применен
        скэппинг из тех, которые запрашивал пользователь.

        Returns:
            list: Список доменов, которые содержат текстовую информацию о страниц внутри себя.
        """
        user_request = self.requests.find_one({'_id': self.user_request_id})
        user_request = user_request if user_request else {}
        scraped_domains = user_request.get('scraped_domains', [])

        return scraped_domains

    def get_keywords(self):
        """Метод, позволяющий получить ключевые слов, по которым пользователь запросил
        проверку.

        Returns:
            pd.DataFrame: Датафрейм содержащий информации о ключевых словах.
        """
        user_request = self.requests.find_one({'_id': self.user_request_id})
        user_request = user_request if user_request else {}
        user_keywords = user_request.get('keywords', {})

        if user_keywords:
            keywords_df = pd.DataFrame.from_dict(data=user_keywords, orient='index')
            keywords_df['Комбинации'] = keywords_df.apply(
                lambda x: self.find_word_combinations(x), axis=1)
        else:
            keywords_df = pd.DataFrame()

        return keywords_df

    def vectorize_page_text(self, page_text):
        """Метод, позволяющий посчитать частотные характеристики терминов в тексте сайтов домена организации.
        Составляются три корпуса **n_gram**: *unigram*, *bigram*, *trigram*.
        Далее по каждому корпусу считаются статистические метрики для кажого термана,
        с помощью метода :func:`~fill_df`.

        Args:
            page_text (str): Склеенный текст страниц сайта.

        Returns:
            dict: Словарь с датафреймами, содержащими информацию о соотвествуюих корпусах n_gram.
        """
        vectorized_texts = {}

        for ngram in range(1, 4):
            cv = CountVectorizer(ngram_range=(ngram, ngram))
            word_count = cv.fit_transform([page_text])

            vector = word_count[0]
            page_df = pd.DataFrame(data=vector.T.todense(), index=cv.get_feature_names(),
                                   columns=["count"])
            page_df = page_df.sort_values(by=["count"], ascending=False)
            page_df = self.fill_df(page_df)
            vectorized_texts['{}_gram'.format(ngram)] = page_df.to_dict(orient='index')

        return vectorized_texts

    def count_domain_statistics(self, page_text):
        """Метод, позволяющий получить статистическую информацию из текста домена организации.
        Подсчет осуществляется на корпусе *unigram*.

        Args:
            page_text (str): Склеенный текст страниц сайта.

        Returns:
            dict: Словарь с статистичесими метриками по терминам текста страниц данной организации.
        """
        cv = CountVectorizer(ngram_range=(1, 1))
        word_count = cv.fit_transform([page_text])

        vector = word_count[0]
        page_df = pd.DataFrame(data=vector.T.todense(), index=cv.get_feature_names(),
                               columns=["count"])
        page_df = page_df.sort_values(by=["count"], ascending=False)

        words_count = page_df['count'].sum()
        words_mean = page_df['count'].mean()
        words_median = page_df['count'].median()
        words_1_quartile = page_df['count'].quantile(0.25)
        words_3_quartile = page_df['count'].quantile(0.75)

        result = {'words_count': int(words_count),
                  'words_mean': float(words_mean),
                  'words_median': float(words_median),
                  'words_1_quartile': float(words_1_quartile),
                  'words_3_quartile': float(words_3_quartile)}

        return result

    @staticmethod
    def fill_df(df):
        """**Статический** метод, позволющий посчитать для каждого термина статистические метрики,
        такие, как: **кол-во вхождений терминов**, **частоты терминов**, **релевантность термина**

        Args:
            df (pd.Dataframe): Датафрейм состоящих из слов и их встречаемости в тексте.

        Returns:
            pd.Dataframe: Датафрейм с посчитанными метриками.
        """
        result_df = df.copy()
        words_count = result_df['count'].sum()

        result_df['word_prec'] = (result_df['count'] / words_count) * 100
        result_df['tf'] = result_df['count'] / sum(result_df['count'])
        result_df['relevance'] = result_df['tf'] / result_df['tf'].mean()

        return result_df

    def count_keywords_relevance(self, vectors, statistics, keywords_df, by='tf'):
        """Метод, который позволяет составить датафрейм, отращающих релевантность данных ключевых
        слов по отношению тексту, путем вызова метода :func:`~find_word_relevance`.

        Args:
            vectors (dict): Словарь, с ключами в виде кол-ва **n_gram** и значениями в виде
                датафрема, с посчитанными частотными метриками для терминов.
            statistics (dict): Словарь с посчитанными статистическими метриками для терминов
                в тексте сайтов данного домена организации.
            keywords_df (pd.DataFrame): Датафрейм, содержащих необходимую информацию о ключевых
                словах.
            by (str): Строка, определяющая колонку для расчета релевантности.

        Returns:
            tuple: Кортеж, содержащий:

                **pd.DataFrame**: Результирующий датафрейм, в котором содержится
                информация о релевантности данных ключевых слов по отношению к тексту
                страниц домена.

                **float**: Суммарная релевантность ключевых слов по отношению к тексту
                страниц домена. Ключевые слова, помеченные как *стоп-слова* входят в сумму
                с отрицательным значением.
        """
        relevance_df = pd.DataFrame(columns=['Ключевое слово',
                                             'Кол-во вхождений ключевого слова',
                                             'Общее кол-во слов',
                                             'Среднее кол-во вхождения слов',
                                             'Первый квартиль вхождения слов',
                                             'Медиана вхождения слов',
                                             'Третий квартиль вхождения слов',
                                             'Критерий поиска ключевого слова',
                                             'Приведенная частота вхождения ключевого слова',
                                             'Вес ключевого слова',
                                             'Приведенная частота вхождения ключевого слова с учетом веса',
                                             'Кол-во вхождений ключевого слова с учетом веса'])

        is_zeroing_out = False

        for i, (word, row) in enumerate(keywords_df.iterrows()):
            word_relevance_df = pd.DataFrame(columns=['word_prec', 'count',
                                                      'word_prec_with_weight', 'count_with_weight'])
            vectorized_df = vectors['{}_gram'.format(row['Кол-во слов'])]
            keyword_criteria = row['Критерий поиска ключевого слова']
            keyword_weight = row['Вес ключевого слова']

            domain_word_count = statistics['words_count']
            domain_words_mean = statistics['words_mean']
            domain_words_median = statistics['words_median']
            domain_words_1_quartile = statistics['words_1_quartile']
            domain_words_3_quartile = statistics['words_3_quartile']

            for keyword_stem in row['Комбинации']:
                stem_lem_relevance_and_count = self.find_word_relevance_and_count(vectorized_df,
                                                                                  keyword_stem)
                if keyword_criteria == 's':
                    if keyword_weight != -1.0:
                        stem_lem_relevance_and_count['word_prec_with_weight'] = stem_lem_relevance_and_count['word_prec'] * -1
                        stem_lem_relevance_and_count['count_with_weight'] = stem_lem_relevance_and_count['count'] * -1
                    else:
                        if stem_lem_relevance_and_count['count'].sum() > 0:
                            is_zeroing_out = True
                else:
                    stem_lem_relevance_and_count['word_prec_with_weight'] = stem_lem_relevance_and_count['word_prec']
                    stem_lem_relevance_and_count['count_with_weight'] = stem_lem_relevance_and_count['count']

                word_relevance_df = word_relevance_df.append(stem_lem_relevance_and_count)

            word_relevance_df = word_relevance_df.drop_duplicates()
            word_relevance_sum = float(word_relevance_df['word_prec'].sum())
            word_count_sum = float(word_relevance_df['count'].sum())
            word_relevance_sum_with_weight = float(word_relevance_df['word_prec_with_weight'].sum() * keyword_weight)
            word_count_sum_with_weight = float(word_relevance_df['count_with_weight'].sum() * keyword_weight)

            data = [word, word_count_sum, domain_word_count,
                    domain_words_mean, domain_words_1_quartile,
                    domain_words_median, domain_words_3_quartile,
                    keyword_criteria, word_relevance_sum,
                    keyword_weight, word_relevance_sum_with_weight,
                    word_count_sum_with_weight]

            relevance_df.loc[i] = data

        if by == 'occurance':
            column_for_relevance = 'Кол-во вхождений ключевого слова с учетом веса'
        else:
            column_for_relevance = 'Приведенная частота вхождения ключевого слова с учетом веса'

        relevance_df = relevance_df.sort_values(column_for_relevance,
                                                ascending=False)

        if is_zeroing_out:
            relevance_sum = 0.0
        else:
            relevance_sum = relevance_df[column_for_relevance].sum()
            if relevance_sum < 0.0:
                relevance_sum = 0.0

        return relevance_df, relevance_sum

    @staticmethod
    def find_word_relevance_and_count(page_df, word):
        """**Статический** метод, который считает релевантность конкретного термина в рамках
        корпуса из соответствующих **n_gram**.

        Args:
            page_df (pd.DataFrame): Датафрейм n_gram текста и их статистических метрик.
            word (str): Термир, по которому необходимо найти релевантность.

        Returns:
            float: Релевантность данного термина.
        """
        value = page_df.loc[page_df.index.str.contains(word)]
        if not value.empty:
            return value[['word_prec', 'count']]
        else:
            return pd.DataFrame(data=[[0, 0]], columns=['word_prec', 'count'], index=[word])

    @staticmethod
    def org_already_contains_calculations(document):
        """**Статический** метод проверки содержания корпусов **n_gram** с посчитанными
        статистическими метриками в базе данных.

        Args:
            document (dict): Документ коллекции MongoDB, содержащий информацию о домене, в том числе
                и корпуса **n_gram**, если ранее домен уже проходил стадию обработки.

        Returns:
            dict: Словарь, с ключами в виде кол-ва **n_gram** и значениями в виде
            датафрема, с посчитанными статистическими метриками для терминов.
        """
        raw_relevance = document.get('raw_word_relevance', {})
        domain_statistics = document.get('domain_statistics', {})

        return raw_relevance, domain_statistics

    def find_relevance_tables(self, scraped_domains, keywords_df):
        """Метод, поиска релевантности ключевых слов относительно доменов. Включает в себя вызов
        всех предыдущих методов для расчета показатлей.

        Args:
            scraped_domains (list): Список доменов, успешно прошедших скрэппинг.
            keywords_df (pd.DataFrame): Датафрейм, содержащих необходимую информацию о ключевых
                словах.

        Returns:
            list: Список, состоящий из словарей, содердащих информацию о домене,
            результирующий датафрейм, в котором содержится информация о релевантности
            данных ключевых слов по отношению к тексту страниц домена и сумму релевантности
            ключевых слов.
        """

        domains_relevance_list = []
        for domain in scraped_domains:
            try:
                org_document = self.organisations.find_one({'domain': domain})
                vectorized_texts, domain_statistics = self.org_already_contains_calculations(org_document)

                if vectorized_texts and domain_statistics:
                    self.logger.add_log('Успешно найдет корпус для домена "{}"'.format(domain))
                else:
                    self.logger.add_log('Началось составление корпуса для домена "{}"'.format(domain))

                    full_site_text = self.get_full_text(org_document)
                    vectorized_texts = self.vectorize_page_text(full_site_text)
                    domain_statistics = self.count_domain_statistics(full_site_text)

                    self.organisations.update_one({'domain': domain},
                                                  {'$set': {'raw_word_relevance': vectorized_texts,
                                                            'domain_statistics': domain_statistics}},
                                                  upsert=False)
                    self.logger.add_log('Корпус для домена "{}" успешно составлен'.format(domain))

                for key, vector in vectorized_texts.items():
                    vectorized_texts[key] = pd.DataFrame.from_dict(vector, orient='index')

                relevance_by_word, relevance_sum = self.count_keywords_relevance(vectorized_texts,
                                                                                 domain_statistics,
                                                                                 keywords_df,
                                                                                 by='occurance')

                domains_relevance_list.append({'domain': domain,
                                               'relevance_by_word': relevance_by_word,
                                               'relevance_sum': relevance_sum})

                domains_relevance_list = sorted(domains_relevance_list,
                                                key=lambda k: k['relevance_sum'],
                                                reverse=True)

                self.logger.add_log('Домен "{}" успешно обработан'.format(domain))

            except Exception as err:
                print(repr(err), ''.join(traceback.format_tb(err.__traceback__)))
                self.logger.add_log('Ошибка при обработке домена {}. {}. Детали: {}'
                                    .format(domain, repr(err), ''.join(traceback.format_tb(err.__traceback__))))
                continue

        return domains_relevance_list
