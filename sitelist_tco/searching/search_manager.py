import requests
from bs4 import BeautifulSoup
from googleapiclient.discovery import build


class SearchManager:
    """Класс ``SearchManager`` используется для формирования запросов и поиска страниц
    удовлетворяющих параметрам запроса.

    Args:
        engines (dict): Словарь в котором хранятся имена используемых поисковых машин, а также
            адреса и формат представления результатов поиска для взяамодействия с ними.
        results_number (int): Кол-во результатов сбора с одного запроса к одной поисковой машине.
        headers (string): Кофнфигурация для отобращения результатов поиска
            как для desktop устройства.

    Attributes:
        engines (dict): Словарь в котором хранятся имена используемых поисковых машин, а также
            адреса и формат представления результатов поиска для взяамодействия с ними.
        results_number (int): Кол-во результатов сбора с одного запроса к одной поисковой машине.
        headers (string): Кофнфигурация для отобращения результатов поиска
        как для desktop устройства.
        my_api_key (str): Ключ для доступа к API Google
        my_cse_id (str): Ключ для доступа к Google Custom Search
        service (GmailService): Объект позволяюший взаимодейтвовать с Google API.
    """
    def __init__(self, headers, results_number, engines):
        self.engines = engines
        self.headers = headers
        self.results_number = results_number

        self.requests = {}

        # self.my_api_key = "AIzaSyD7Q9gw7s3PJh3miU4q5DKVwvDuw4ZETCM"
        self.my_api_key = "AIzaSyC96GUAQ0kSRADM_f12eVyNo2qsrl00R4s"
        # self.my_cse_id = "cca7bae9c28ccd6b3"
        self.my_cse_id = "a299a85982903ac39"
        self.service = build("customsearch", "v1", developerKey=self.my_api_key)

    def make_search_through_google_api(self, search_term, start):
        """Метод производит поиск с помошью поисковой с помощью Google Custom Search API.

        Returns:
            list: Результатом является список с результатами поиска. В качетсве элементов списка
            сожержаться словари, хранящие в себе адрес и заголовок сайта.
        """
        response = self.service.cse().list(q=search_term, num=10, start=start, cx=self.my_cse_id).execute()
        results = response.get('items', [])

        result_sites = []
        for result in results:
            item = {
                "title": result.get('snippet', 'Неизвестно'),
                "link": result.get('link', 'Неизвестно')
            }
            result_sites.append(item)
        return result_sites

    def make_search(self, url, engine_type):
        """Метод производит поиск с помошью поисковой системы с учетом заранее
        определенных атрибутов экземпряла класса ``SearchEngine``.

        Returns:
            list: Результатом является список с результатами поиска. В качетсве элементов списка
            сожержаться словари, хранящие в себе адрес и заголовок сайта.
        """

        resp = requests.get(url=url, headers=self.headers)
        if resp.status_code == 200:
            return self.get_links_from_html(page_source=resp.content, engine_type=engine_type)

    def get_links_from_html(self, page_source, engine_type):
        """Метод для вычленения адресов и заголовков ресурсов из поисковой выдачи.

        Args:
            page_source (str): Содержимое HTML документа поисковой выдачи.
            engine_type (dict): Словарь типа поисковой машины. Используется
                для определения разметки, которую нужно распарсить.

        Returns:
            list: Список адресов и их заголовков в том же порядке, как и на странице выдачи.
        """
        # engine_search_repr = self.engines[engine_type]['search_repr']

        type_of_block_with_link = self.engines[engine_type]['search_repr']['link_block']['type']
        class_of_block_with_link = self.engines[engine_type]['search_repr']['link_block']['class']
        type_of_header_with_name = self.engines[engine_type]['search_repr']['header_type']

        soup = BeautifulSoup(page_source, "html.parser")
        results = []
        for link_block in soup.find_all(type_of_block_with_link, class_=class_of_block_with_link,
                                        limit=self.results_number):
            anchors = link_block.find_all('a')
            if anchors:
                link = anchors[0]['href']
                title = link_block.find(type_of_header_with_name).text
                item = {
                    "title": title,
                    "link": link
                }
                results.append(item)
        return results

    def create_request(self, request_fmt, list_of_requests, parameters, separator):
        """Метод для формирования адреса запроса.

        Args:
            request_fmt (str): Шаблон запроса конкретной поисковой машины.
            list_of_requests (list): Список запросов, который пополнятеся в ходе работы метода.
            parameters (list): Список параметров Организации, указанных во вложеннии.
            separator (str): Разделитель слов конкретной поисковой машины.
        """
        # Добавляем 10 страниц к результатам, чтобы кол-во результатов было тольно нужное
        num_of_results = int(self.results_number) + 10
        parameters_list = []

        # for parameter in parameters:
        #     if parameter:
        #         parameters_list.append(parameter.replace(' ', separator))
        # parameters_list = separator.join(parameters_list)
        # result_request = request_fmt.format(parameters_list, num_of_results)

        parameters_list = ' '.join(parameters)

        result_request = request_fmt.format(parameters_list)
        list_of_requests.append(result_request)

    def create_requests_list(self, query_params):
        """Метод для формирования списка адресов,
        по которым в дальнейшем будут совершаться GET запросы.

        Args:
            query_params (dict): Словарь с параметрами для поиска конкретной организации,
                взятый из вложения к сообщению.

        Returns:
            dict: Словарь в котором соедержаться комбинации запросов для разных организаций,
            с использованием разных поисковых машин. Ключ - наименование поисковой машины,
            значение - словарь, где ключ - индекс запроса по организации, значение -
            комбинации запросов поиска по этой организации.
        """
        requests_by_each_search_engine = {}

        for search_engine in self.engines.keys():
            requests_by_each_search_engine.update({search_engine: {}})

            for index, query in query_params.items():
                requests_to_search_engine = []

                name = query['name']
                inn = query['inn']
                address = query['address']
                description = query['description']

                if name:
                    self.create_request(
                        request_fmt=self.engines[search_engine]['request_fmt'],
                        list_of_requests=requests_to_search_engine,
                        parameters=[name],
                        separator=self.engines[search_engine]['search_params']['word_separator']
                    )

                    if inn:
                        self.create_request(
                            request_fmt=self.engines[search_engine]['request_fmt'],
                            list_of_requests=requests_to_search_engine,
                            parameters=[inn, name],
                            separator=self.engines[search_engine]['search_params']['word_separator']
                        )

                    if address:
                        self.create_request(
                            request_fmt=self.engines[search_engine]['request_fmt'],
                            list_of_requests=requests_to_search_engine,
                            parameters=[name, address],
                            separator=self.engines[search_engine]['search_params']['word_separator']
                        )

                    if description:
                        self.create_request(
                            request_fmt=self.engines[search_engine]['request_fmt'],
                            list_of_requests=requests_to_search_engine,
                            parameters=[name, description],
                            separator=self.engines[search_engine]['search_params']['word_separator']
                        )

                requests_by_each_search_engine[search_engine].update({index: requests_to_search_engine})
        return requests_by_each_search_engine
