import io
import traceback
from pprint import pprint

import pandas as pd


class OutputAttHelper:
    """Класс служит для формирования выходного excel файла.

    Attributes:
           dict_to_attach (dict): Словарь, в котором для каждого адреса по
               конкретным запросам сформирован средний рейтинг
               на основе списка рейтингов воисковой выдачи.
           user_logger (Logger): Объект позволяющий логировать события по конкретному пользователю.
    """

    def __init__(self, dict_to_attach, user_logger):
        self.dict_to_attach = dict_to_attach
        self.user_logger = user_logger

    def create_att(self, spaces=0, header=2):
        """Создание вложения на основе словаря из ``df_by_query_dict``.

        Args:
            spaces (int): Размер (в строках) промежутся между таблицами результатов в excel.
                По умолчанию - 1.

        Returns:
            bytearray: Результирующий вложенный excel файл в виде набора байтов.
        """
        try:
            output = io.BytesIO()
            writer = pd.ExcelWriter(output, engine='xlsxwriter')

            workbook = writer.book
            worksheet = workbook.add_worksheet('Результат')
            writer.sheets['Результат'] = worksheet

            formats = self.get_file_formats(workbook=workbook)


            # Добавляем шапку для отчета
            worksheet.merge_range('A1:E1', 'Результат поиска сайтов по ключевым словам', formats['main_header_format'])

            row = 3
            max_widths_list = [20, 30, 35, 35, 20, 50, 100, 15]

            inn_col = 0
            name_col = 1
            address_col = 2
            description_col = 3

            search_engine_name_col = 4
            search_engine_result_start_col = 5

            for query_index, result_dict in self.dict_to_attach.items():
                header_columns = ['ИНН Организации', 'Наименование Организации',
                                  'Юридический Адрес Организации', 'Описание',
                                  'Поисковик', 'Адрес Ресурса', 'Заголовок', 'Рейтинг Адреса']
                if query_index == 0:
                    for col_index, col_name in enumerate(header_columns):
                            worksheet.write(row, col_index, col_name, formats['header_format'])
                # else:
                #     for col_index, col_name in enumerate(header_columns):
                #         worksheet.write(row, col_index, formats['header_format'])
                start_row = row + 1
                row = row + 1
                search_engines = result_dict['search_engines']
                for search_engine, dataframe in search_engines.items():

                    # Для пустых датафреймов создаем сообщение о неспушном поиске
                    if len(dataframe.index) == 0:
                        dataframe = dataframe.append([{'Адрес ресурса': 'Не нашлось не одного подходящего ресурса',
                                                       'Заголовок': '', 'Рейтинг Адреса': ''}])

                    dataframe.to_excel(writer, sheet_name=worksheet.name,
                                       startrow=row, startcol=search_engine_result_start_col,
                                       index=False, header=False)

                    if len(dataframe.index) > 1:
                        worksheet.merge_range(first_row=row, first_col=search_engine_name_col,
                                              last_row=row + len(dataframe.index) - 1, last_col=search_engine_name_col,
                                              data='', cell_format=formats['ws_center_format'])

                    worksheet.write(row, search_engine_name_col, search_engine, formats['ws_center_format'])
                    row = row + len(dataframe.index)

                worksheet.write(start_row, name_col, result_dict['name'])

                columns_merged = {'inn': inn_col, 'name': name_col,
                                  'address': address_col, 'description': description_col}
                if (row - 1) - start_row > 1:
                    for column_name, column_value in columns_merged.items():
                        if column_name in ['address', 'description']:
                            format_ = formats['merge_about_format']
                        else:
                            format_ = formats['ws_center_format']
                        worksheet.merge_range(first_row=start_row, first_col=column_value,
                                              last_row=row - 1, last_col=column_value,
                                              data='', cell_format=format_)

                for column_name, column_value in columns_merged.items():
                    worksheet.write(start_row, column_value, result_dict[column_name])

                row = row + spaces-1

            for i, width in enumerate(max_widths_list):
                if i == 6:
                    worksheet.set_column(i, i, width, cell_format=formats['ws_left_format'])
                else:
                    worksheet.set_column(i, i, width, cell_format=formats['ws_center_format'])

            writer.save()
            xlsx_data = output.getvalue()

            self.user_logger.add_log('Успешно создано вложение с результатами.')
            return xlsx_data

        except Exception as err:
            self.user_logger.add_log(
                'Ошибка при создании вложения с результатами: {}. Детали: {}.'.format(
                    repr(err), ''.join(traceback.format_tb(err.__traceback__))
                )
            )

    @staticmethod
    def get_file_formats(workbook):
        link_format = workbook.get_default_url_format()
        link_format.set_align('left')
        link_format.set_valign('vcenter')

        ws_left_format = workbook.add_format({'align': 'left', 'valign': 'vcenter'})
        ws_center_format = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'text_wrap': True})

        header_format = workbook.add_format({
            'bold': True,
            'text_wrap': True,
            'fg_color': '#305496',
            'border': 1,
            'align': 'center',
            'valign': 'center',
            'font_color': 'white'})

        main_header_format = workbook.add_format({
            'bold': True,
            'text_wrap': True,
            'bg_color': '#305496',
            'align': 'left',
            'font_size': '16',
            'valign': 'left',
            'font_color': 'white'})

        merge_common_format = workbook.add_format({'align': 'center', 'valign': 'top', 'text_wrap': True})

        formats = {'ws_left_format': ws_left_format,
                   'ws_center_format': ws_center_format,
                   'main_header_format': main_header_format,
                   'header_format': header_format,
                   'merge_about_format': merge_common_format}

        return formats
