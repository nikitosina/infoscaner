import datetime
import email.mime.application
import imaplib
import smtplib
import time
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from io import BytesIO

from imap_tools import MailBox, MailMessageFlags, AND

from exceptions.exceptions import ConnctionToMailError


class MailBoxer:
    """Класс :class:`~MailBoxer` используется для работы с почтой.
    Он реализует методы по прочтению, предварительной обработки вложений и отправлению сообщений.
    Attributes:
        mail_host (str): Адрес хоста для подключения к почте через IMAP.
        login (str): Логин для авторизации в почте.
        password (str): Пароль для авторизации в почте.
        mailbox (MailBox): Клиент почтового сервиса IMAP.
        system_logger (:class:`~logs.logs_helper.Logger`): Логгер для общих отслеживания общих сообщений до
            начала работы с инвидуальными запросами пользователей.
    """

    def __init__(self, logger):
        self.mail_host = 'imap.mail.ru'
        self.login = 'find_organisations@mail.ru'
        self.password = 'LKn-r4J-YRM-Upg'

        self.system_logger = logger

        self.mailbox = None

    def connect_mailbox(self):
        try:
            self.mailbox = MailBox(host=self.mail_host)
            self.mailbox.login(self.login, self.password, initial_folder='INBOX')
        except Exception:
            raise ConnctionToMailError

    def get_messages(self, move_to_archive=True):
        """Метод, позволяющий сформировать список непрочитанных сообщений.
        Args:
            move_to_archive (bool) : Если ``True``, то переносит прочитанные сообщения
                в архив с помощью метода :func:`~move_to_archive`, если ``False``,
                то отсавляет их без изменения.
        Returns:
            list: Список сообщений, отфильтрованных по наличию вложения.
        """

        # if move_to_archive:
        #     flags = (MailMessageFlags.SEEN, MailMessageFlags.FLAGGED)
        #     self.mailbox.flag(self.mailbox.fetch(AND(seen=False)), flags, True)

        msgs = [msg for msg in self.mailbox.fetch(AND(seen=False))]
        filtered_msgs = []

        if not msgs:
            self.system_logger.add_log('Новых писем не найдено')
        else:
            self.system_logger.add_log('Найдено сообщений: {}'.format(len(msgs)))
            if move_to_archive:
                self.move_to_archive(msgs)
            filtered_msgs = self.filtered_msgs(msgs)
            self.system_logger.add_log('Сообщений после фильтрации: {}'.format(len(msgs)))
        return filtered_msgs

    def move_to_archive(self, msgs):
        """Метод, позволяющий перенести список сообщений в архив.
        Args:
            msgs (list): Список сообщений для переноса в ахрив.
        """
        for msg in msgs:
            try:
                self.mailbox.move([msg.uid], 'Archive')
            except ConnectionResetError as err:
                self.system_logger.add_log('Соединение с почтой потеряно, восстанавливаем: {}.'
                                           .format(repr(err)))
                try:
                    self.connect_mailbox()
                except Exception:
                    raise ConnctionToMailError
                else:
                    self.mailbox.move([msg.uid], 'Archive')

        self.system_logger.add_log('Все сообщения перенесены в архив.')

    def filtered_msgs(self, msgs):
        """Метод, позволяющий отобрать сообщения, в которых содержится вложения, для дальнейшей
        обработки. Для тех сообщений, которые не прошли проверку, создается документ с помощью
        метода :func:`~create_user_document` и заносится в коллекцию ``requests_coll``, а также
        создается сообщение с информацией о проблеме с помошью метода :func:`~create_msg`,
        которое в затем посылается пользователю через метод :func:`~send_mail`.
        Args:
            msgs (list): Список сообщений для проверки.
        Returns:
            list: Список сообщений, прошедших проверку.
        """
        filtered_msgs = []
        for msg in msgs:
            msg_content = self.get_data_from_msg(msg)

            if not msg_content['input_attachment']:
                error_mail = self.create_msg(msg_content)
                self.send_mail(error_mail)

                user_logger = self.system_logger.copy()
                user_logger.add_log('Отсутствует вложенный файл')

                user_logger.push_logs(msg_content['from'],
                                      msg_content['subject'],
                                      msg_content['received_time'])
            else:

                # Берем только первое вложение
                msg_content['input_attachment'] = BytesIO(
                    msg_content['input_attachment'][0].payload
                )
                filtered_msgs.append(msg_content)

        return filtered_msgs

    @staticmethod
    def get_data_from_msg(msg):
        """**Статический** метод, возволяющий вычленить из объекта сообщения, данные и составить
        словарь.
        Ключи, содержащиеся в словаре:
            - **from**: Отправитель
            - **subject**: Тема сообщения
            - **recieved_time**: Время получения сообщения
            - **input_attachment**: Полученный вложенный файл в байтах
            - **result_attachment**: Выходной вложенный файл, пока что None
            - **errors**: Список ошибок возникших при обработке данного сообщения
        Args:
            msg (MailBox.Message): Объект сообщения.
        Returns:
            msg_content: Выходной словарь.
        """
        msg_content = {}

        msg_from = msg.from_
        # Приводим к времени по МСК
        msg_received_time = msg.date + datetime.timedelta(hours=3)
        msg_subject = msg.subject
        msg_att = msg.attachments

        msg_content.update({
            'from': msg_from,
            'subject': msg_subject,
            'received_time': msg_received_time,
            'input_attachment': msg_att,
            'result_attachment': None,
            'errors': []
        })

        return msg_content

    def create_msg(self, msg_content):
        """Метод, который служит для формирования сообщения.
        Заполняются поля ``from``, ``to``, ``subject``, ``attachment``, ``message text``.
        Args:
            msg_content (dict): Cловарь с содержимым сообщения, вида.
        Returns:
            MIMEMultipart: Содержимое сообщения
        """
        msg = MIMEMultipart()
        msg['From'] = self.login
        msg['To'] = msg_content['from']
        if msg_content['subject']:
            msg['Subject'] = 'Результат обработки запроса по теме: ' + msg_content['subject']
        else:
            msg['Subject'] = 'Результат обработки запроса'

        if not msg_content['errors']:
            # The main body is just another attachment
            body = MIMEText('Результат обработки находится в приложенном файле.')
            msg.attach(body)

            if msg_content['result_attachment']:
                att = email.mime.application.MIMEApplication(msg_content['result_attachment'],
                                                             _subtype="xlsx")
                att.add_header('Content-Disposition', 'attachment', filename='Результат.xlsx')
                msg.attach(att)
        else:
            if msg_content['result_attachment']:
                msg_text = 'Результат обработки находится в приложенном файле.\n\n' \
                           + 'Ошибки:\n' + '\n\t'.join(msg_content['errors'])
                body = MIMEText(msg_text)
                msg.attach(body)
                att = email.mime.application.MIMEApplication(msg_content['result_attachment'],
                                                             _subtype="xlsx")
                att.add_header('Content-Disposition', 'attachment', filename='Результат.xlsx')
                msg.attach(att)
            else:
                msg_text = 'Ошибки:\n' + '\n\t'.join(msg_content['errors'])
                body = MIMEText(msg_text)
                msg.attach(body)
        return msg

    def send_mail(self, msg):
        """Метод, позволяющий организовать отправку сообщения через SMTP. А также автоматический
        перенос данного сообщения в архив через IMAP.
        Args:
            msg (MIMEMultipart): Содержимое сообщения.
        """
        try:
            sender = self.connect_to_smtp()
            sender.starttls()
            sender.login(self.login, self.password)
            sender.sendmail(self.login, msg['To'], msg.as_string())
        except Exception:
            raise ConnctionToMailError
        else:
            # Поместить отправленное в архив
            try:
                imap = imaplib.IMAP4_SSL(self.mail_host)
                imap.login(self.login, self.password)
                imap.append('Archive', flags='\\Seen', date_time=imaplib.Time2Internaldate(time.time()),
                            message=msg.as_string().encode('utf8'))
                imap.logout()
            except Exception:
                raise ConnctionToMailError
            finally:
                sender.quit()

        self.system_logger.add_log('Почтовое сообщение отправлено успешно')

    @staticmethod
    def connect_to_smtp():
        try:
            sender = smtplib.SMTP('smtp.mail.ru', port=587)
        except Exception:
            sender = smtplib.SMTP('smtp.mail.ru', port=465)
        return sender
