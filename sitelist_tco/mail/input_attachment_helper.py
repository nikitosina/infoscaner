#coding: utf-8


import traceback
from pprint import pprint

import numpy as np
import pandas as pd

from exceptions.exceptions import ValidationError


class InputAttHelper:
    """Класс `AttHelper` служит для обработки вложенный в email excel файлов.

    Attributes:
        input_attachment (bytearray): Содержимое вложения
        user_logger (Logger): Объект позволяющий логировать события по конкретному пользователю.
        error_list (list): Список ошибок конкретного сообщения - msg_details['errors'].
    """

    def __init__(self, input_attachment, errors_list, user_logger):
        self.input_attachment = input_attachment
        self.user_logger = user_logger
        self.errors_list = errors_list

    def parse_att(self):
        """Функция `parse_att` формирует `pandas dataframe` из вложенного файла,
        переименовывает столбцы и возвращиет словарь с содержимым.

        Returns:
            dict: Словарь с содержимым вложенного файла.
        """
        try:
            df_att = pd.read_excel(self.input_attachment, header=5, sheet_name='Запрос', dtype=str).iloc[:, 1:]
            df_att = df_att.rename(columns={
                "Параметр 1": "ИНН Организации",
                "Параметр 2": "Наименование организации",
                "Параметр 3": "Юридический адрес организации",
                "Параметр 4": "Описание",
            })
            df_att.rename(columns={'ИНН Организации': 'inn',
                                   'Наименование организации': 'name',
                                   'Юридический адрес организации': 'address',
                                   'Описание': 'description'}, inplace=True)

            df_att = df_att.dropna(how='all')
            df_att = df_att.dropna(axis=1, how='all')

            df_att = df_att.fillna(value='')

            self.validate_df(df_to_validate=df_att)

            result = df_att.to_dict(orient='index')

            self.user_logger.add_log('Вложение успешно обработано.')

            return result

        except ValidationError as err:
            self.user_logger.add_log(
                'Ошибка при валидации вложения: {}. Детали: {}'.format(
                    repr(err), ''.join(traceback.format_tb(err.__traceback__))
                )
            )
            self.errors_list.append(err.msg)

        except Exception as err:
            self.user_logger.add_log(
                'Ошибка при обработки вложения: {}. Детали: {}'.format(
                    repr(err), ''.join(traceback.format_tb(err.__traceback__))
                )
            )
            self.errors_list.append('Не удалось обработать вложение,'
                                    ' проверьте его на соответсвие шаблону')

    def validate_df(self, df_to_validate):
        orgs_number = len(df_to_validate)
        if orgs_number > 250:
            raise ValidationError(msg='Слишком много организаций для поиска {},'
                                      ' максимальное значение: 250'.format(orgs_number))
        elif orgs_number == 0:
            raise ValidationError(msg='Не нашлось ни одной организации для поиска')
