class ConnctionToMailError(Exception):
    """Класс :class:`~ConnctionToMailError` предназначен для обработки исключений, произощедших во
    время подключения к почтовому сервису.
    Attributes:
        msg (str): Строка с сообщением об ошибке.
    """

    def __init__(self, msg=None):
        if not msg:
            self.msg = 'Ошибка подключения к почтовому сервису со стороны приложения.'
        else:
            self.msg = msg
        super().__init__(self.msg)


class ValidationError(Exception):
    """Класс :class:`~ValidationError` предназначен для обработки исключений, произощедших во время
    прочтения и преобразования вложенного файла в экземпляре класса :class:`~attachment_helper`
    Attributes:
        msg (str): Строка с сообщением об ошибке.
    """

    def __init__(self, msg=None):
        if not msg:
            self.msg = 'Ошибка валидации вложенного файла. ' \
                       'Убедитесь, что он соответствует шаблону.'
        else:
            self.msg = msg
        super().__init__(self.msg)
