from datetime import datetime
from pymongo import MongoClient


class Logger:
    """Класс `Logger` позволяет логировать успешные шаги работы программы,
    а также, возникающие ошибки.

    Attributes:
        client (MongoClient): Клиент MongoDB для взаимодеяствия с БД.
        db_logs (MongoDataBase): База логов.
        sitelist_logs (MongoCollection): Коллекция логов данной программы.
        logs_list (list): Список логов за время работы одного процесса.
        from_ (str): Параметр, который определяет поле "from" документа в коллекции.
            Обозначает пользователя, при обработки почтового сообщения которого,
            были собраны логи.
            Служит создания различных документов и ориентирования в них.
        subj_ (str): Параметр, который определяет поле "subject" документа в коллекции.
            Обозначает тему, сообщения пользователья `from_`.
            Служит создания различных документов и ориентирования в них.
        received_time (datetime): Время получения сообщения от пользователя.
    """

    def __init__(self, from_=None, subj_=None, received_time='unknown'):
        self.client = MongoClient(host='79.174.70.192',
                                  port=32412,
                                  username='mongo-root',
                                  password='J*f0-wb&8c',
                                  authSource='admin',
                                  authMechanism='SCRAM-SHA-1')
        self.db_logs = self.client['logs']
        self.sitelist_logs = self.db_logs.sitelist_logs
        self.sitelist_instant_logs = self.db_logs.sitelist_instant_logs
        self.logs_list = []

        self.from_ = from_
        self.subj_ = subj_
        self.received_time = received_time

    def add_log(self, msg):
        """Метод, позволяющий дополнять список логов `logs_list` данного процесса,
        дополнительно устанавливает метку времени.

        Args:
            msg (str): Сообщение о проделанном действии или ошибке.
        """
        self.logs_list.append([datetime.now().strftime("%m/%d/%Y, %H:%M:%S"), msg])
        self.instant_log(msg)

    def push_logs(self):
        """Метод, позволяющий отправить список логов в
        коллекцию базы данных.
        """

        self.sitelist_logs.insert_one(
            {'upload_time': datetime.now(),
             'received_time': self.received_time,
             'from': self.from_,
             'subject': self.subj_,
             'details': self.logs_list})
        self.logs_list = []

    def copy(self, from_, subj_, received_time):
        """Метод, позволяет скопировать `logs_list` существующего экземпряра класса в
        аттрибут `logs_list` нового экземпяра.

        Args:
            from_ (str): Параметр, который определяет поле "from" документа в коллекции.
                Обозначает пользователя, при обработки почтового сообщения которого,
                были собраны логи.
                Служит создания различных документов и ориентирования в них.
            subj_ (str): Параметр, который определяет поле "subject" документа в коллекции.
                Обозначает тему, сообщения пользователья `from_`.
                Служит создания различных документов и ориентирования в них.
            received_time (datetime): Время получения сообщения от пользователя.

        Returns:
            Logger: Новый экземпляр класса `Logger` с аттрибутом `logs_list`,
            заполненным из старого экземпляра.
        """
        new_logger = Logger(from_=from_, subj_=subj_, received_time=received_time)
        new_logger.logs_list = self.logs_list[:]
        return new_logger

    def instant_log(self, msg):
        """Метод, позволяющий отправить одиночный логов в
        коллекцию базы данных для мнговенных логов.

        Args:
            msg (str): Сообщение о проделанном действии или ошибке.
        """

        self.sitelist_instant_logs.insert_one(
            {'upload_time': datetime.now().strftime("%m/%d/%Y, %H:%M:%S.%f")[:-3],
             'from': self.from_,
             'subject': self.subj_,
             'details': msg})
