from organisation_finder import OrganisationFinder
from time import time, sleep


if __name__ == '__main__':
    while True:
        finder = OrganisationFinder()
        finder.do_work()
        sleep(60 - time() % 60)