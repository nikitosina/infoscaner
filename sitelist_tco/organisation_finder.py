import json
import os
import re
import time
import traceback
from pprint import pprint
from urllib.parse import urlparse

import pandas as pd
from googleapiclient.errors import HttpError as GoogleHttpError

from mail.input_attachment_helper import InputAttHelper
from mail.output_attachment_helper import OutputAttHelper
from logs.logs_helper import Logger
from mail.mailboxer import MailBoxer
from searching.search_manager import SearchManager
from settings import stop_sites_list, stop_docs_exts


class OrganisationFinder:
    """Класс ``OrganisationFinder`` соединяет работу ``Mailboxer`` и ``SearchManager``.

    Attributes:
            engines (dict): Словарь в котором хранятся имена используемых поисковых машин, а также
                адреса и формат представления результатов поиска для взяамодействия с ними.
            engine_results_number (int): Кол-во результатов сбора с одного запроса к одной
                поисковой машине.
            results_to_send_number (int): Максимальное кол-во ранжированных результатов,
                которое получит пользователь
            depth_of_crawling (int): Кол-во документов одной страницы из выдачи поисковой машины,
                которые будут включены в обработку.
            engine_number (int): Кол-во поисковых машин которое следует использовать.
            headers (string): Кофнфигурация отобращения результатов поиска для desktop устройства.
            mailboxer (MailBoxer): Объект, с помошью которого происходит работа с почтовым сервисом.
    """

    def __init__(self):
        os.chdir(os.path.dirname(os.path.abspath(__file__)))

        self.engines = {
            'google': {'request_fmt': '{}',  # https://google.com/search?q={}&num={}
                       'search_params': {'number_of_results': 'num', 'word_separator': '+'},
                       'search_repr': {
                           "link_block": {
                               "type": "div",
                               "class": "rc"
                           },
                           "header_type": "h3"
                       }}
        }
        self.engine_results_number = None
        self.results_to_send_number = None
        self.depth_of_crawling = None
        self.engine_number = None
        self.headers = None

        self.mailboxer = None
        self.logger = Logger()
        self.get_properties()

    def get_properties(self):
        """Метод, позволяющий определить аттрибуты из файла ``search_properties.json``.
        Служит для настройки поиска пользователем.

        Определяются аттрибуты:
            - ``engine_results_number``
            - ``results_to_send_number``
            - ``depth_of_crawling``
            - ``engine_number``
            - ``headers``
        """
        try:
            with open('search_properties.json', 'r') as file:
                props = json.load(file)

            self.engine_results_number = props['engine_results_number']
            self.results_to_send_number = props['results_to_send_number']
            self.depth_of_crawling = props['depth_of_crawling']
            self.engine_number = props['engine_number']

            self.logger.add_log('Параметры поиска успешно прочитаны и установлены')

        except Exception as err:
            self.logger.add_log('Использование параметров по умолчанию для поиска. '
                                'Ошибка при обработки параметров: {}. Детали: {}'
                                .format(repr(err), ''.join(traceback.format_tb(err.__traceback__))))

            self.engine_results_number = 10
            self.results_to_send_number = 10
            self.depth_of_crawling = 5
            self.engine_number = 1

        finally:
            self.headers = {'user-agent': props['USER_AGENT']}

    def get_messages(self, move_to_archive=True):
        """Метод, позволяющий сформировать список непрочитанных сообщений с помошью ``MailBoxer``.

        Args:
            move_to_archive (bool) : Если ``True``, то помечает выбранные сообщения как прочитанные и переносит в Архив,
                если 'False', то не помечает сообщения как прочитанные.

        Returns:
            list: Список состоящий из словарей с содержимым каждого сообщения, вида::

                {
                    'from': 'email@from.ru',
                    'subject': 'find organisations',
                    'att': '736f6d6520617474617463686d656e74',
                    'errors': []
                }
        """
        msgs = []
        try:
            self.mailboxer = MailBoxer(logger=self.logger)
            self.mailboxer.connect_mailbox()
            self.logger.add_log('Успешное подключение к почте')

        except Exception as err:
            self.logger.add_log('Ошибка при подключении к почте: {}. Детали: {}'
                                .format(repr(err), ''.join(traceback.format_tb(err.__traceback__))))

        try:
            msgs = self.mailboxer.get_messages(move_to_archive=move_to_archive)
            self.logger.add_log('Успешно прочитаны сообщения')

        except Exception as err:
            self.logger.add_log('Ошибка при чтении сообщений: {}. Детали: {}'
                                .format(repr(err), ''.join(traceback.format_tb(err.__traceback__))))

        msgs = [] if not msgs else msgs
        return msgs

    def search_query(self, query_params, error_list, user_logger):
        """Метод, реализующий формирование запросов на основе вложенного файла и их дальнейший
        обработку с помощью объекта класса ``SearchManager``.

        Args:
            query_params (dict): Словарь содержимого вложенного файла - msg_details['att'].
            error_list (list): Список ошибок конкретного сообщения - msg_details['errors'].
            user_logger (Logger): Объект позволяющий логировать события по конкретному пользователю.

        Returns:
            tuple: Кортеж, содержащий:

                **dict**: Результирующий словарь, в котором содержится
                информация о рейтинге адресов для конкретного запроса организации при использовании
                конкретной поисковой машины. Ключ - поисковая машина, значение - словарь, где
                ключ - номер запроса организации, значение - адреса и список рейтингов, которые
                были им присвоены по запросам для данной организации.

                **int**: Кол-во запросов для конкретной огранизации.
        """
        search_manager = SearchManager(engines=self.engines,
                                       headers=self.headers,
                                       results_number=self.engine_results_number)

        search_engine_query_relevance_dict = None
        requests_count_by_query = None

        try:
            all_requests = search_manager.create_requests_list(query_params=query_params)
            user_logger.add_log('Успешно создан список запросов')

        except Exception as err:
            user_logger.add_log('Ошибка при созданнии списка запросов: {}. Детали: {}'
                                .format(repr(err), ''.join(traceback.format_tb(err.__traceback__))))
            error_list.append('По технический причинам произошла ошибка составления запросов')

        else:
            requests_count_by_query, requests_count = self.count_requests(all_requests)
            search_engine_query_relevance_dict = {}

            for search_engine, query in all_requests.items():
                search_engine_query_relevance_dict.update({search_engine: {}})

                for index_of_query, req_list in query.items():
                    search_engine_query_relevance_dict[search_engine].update({index_of_query: {}})
                    # Параметр нужен для конвертации позиции адреса в поисковой выдачи в значение
                    #   рейтинга адреса. Пример: если кол-во рез-ов выдача - 10, тогда
                    #   значение рейтинга для 1-ого адреса будет 10, а для 10-го - 1.
                    num_of_results_plus_one = self.engine_results_number + 1

                    responses_dict = self.create_responses_dict(
                        req_list=req_list,
                        search_manager=search_manager,
                        user_logger=user_logger,
                        error_list=error_list
                    )

                    if responses_dict:
                        if responses_dict == 'api_err':
                            user_logger.add_log('Ошибка Google API. Работа прекращена')
                            error_list.append('Превышен лимит запросов к Google. Работа прекращена')
                            break
                        else:
                            user_logger.add_log('Cобраны результаты поиска для запроса {} из {}.'
                                                .format(index_of_query + 1, len(query)))

                            search_engine_query_relevance_dict[search_engine] \
                                .update({index_of_query: responses_dict})
                    else:
                        user_logger.add_log('Ошибка сбора результатов поиска для запроса {} из {}.'
                                            .format(index_of_query + 1, len(query)))

        return search_engine_query_relevance_dict, requests_count_by_query

    def create_responses_dict(self, req_list, search_manager, user_logger, error_list):
        responses_df_first_page = self.fill_responses_df(
            req_list=req_list,
            search_manager=search_manager,
            user_logger=user_logger,
            error_list=error_list,
            page_num=1
        )

        if len(responses_df_first_page.index) < 3:
            responses_df_second_page = self.fill_responses_df(
                req_list=req_list,
                search_manager=search_manager,
                user_logger=user_logger,
                error_list=error_list,
                page_num=2
            )

            result_df = responses_df_first_page.append(responses_df_second_page)
            result_df['domain'] = result_df['link'].apply(lambda x: urlparse(x).netloc)
            result_df['rating'] = result_df.groupby('domain')['rating'].transform('sum')
            result_df = result_df.drop_duplicates(subset=['domain'], keep='first')
        else:
            responses_df_first_page['rating'] = responses_df_first_page.groupby('domain')['rating'].transform('sum')
            result_df = responses_df_first_page.drop_duplicates(subset=['domain'], keep='first')

        # Опционально можем увеличить рейтинг адреса до 2х раз
        result_df = self.increase_rating_by_length(result_df)
        result_df = result_df.sort_values(by='rating', ascending=False)

        result_df = result_df.set_index(['link']).drop(columns=['domain', 'scheme'])
        result_dict = result_df.to_dict(orient='index')

        return result_dict

    def fill_responses_df(self, req_list, search_manager, user_logger, error_list, page_num):
        """Метод, позволяющий составить словарь результатов поисковой машины.

        Args:
            req_list (list): Список запросов для конкретной организации.
            search_manager (SearchManager): Объект, организующий работу с поисковой машиной.
            user_logger (Logger): Объект позволяющий логировать события по конкретному пользователю.
            error_list (list): Список ошибок конкретного сообщения - msg_details['errors'].

        Returns:
            pd.Dataframe: Датафрейм, заполненный результатами поиска организации по одной странице
            с примененной фильтрацией.
        """
        responses_dict = {}
        req_count = 0
        if len(req_list) == 0:
            responses_dict['Без результатов'] = {'rating_list': [], 'title': '',
                                                 'domain': '', 'scheme': ''}

            err_msg = 'Много пустых данных'
            error_list.append(err_msg) if err_msg not in error_list else error_list
            return 'api_err'
        for req in req_list:
            try:
                # resp_list = search_manager.make_search(url=req, engine_type=search_engine)
                google_page_start = 0
                if page_num == 2:
                    google_page_start = 10
                resp_list = search_manager.make_search_through_google_api(search_term=req, start=google_page_start)

                for index, resp in enumerate(resp_list):
                    relevance_index = index + 1
                    rating_before_correction = (len(resp_list) + 1 - relevance_index) / (len(resp_list) * page_num)
                    link = resp['link']
                    title = resp['title']
                    domain = urlparse(link).netloc
                    scheme = urlparse(link).scheme

                    if link in responses_dict.keys():
                        responses_dict[link]['rating_list'].append(rating_before_correction)
                    else:
                        responses_dict[link] = {'rating_list': [rating_before_correction], 'title': title,
                                                'domain': domain, 'scheme': scheme}

                req_count += 1
                time.sleep(0.1)

            except Exception as err:
                responses_dict['Без результатов'] = {'rating_list': [], 'title': '',
                                                     'domain': '', 'scheme': ''}

                user_logger.add_log(
                    'Ошибка по время поиска: {}. Детали: {}'.format(
                        repr(err), ''.join(traceback.format_tb(err.__traceback__))
                    )
                )
                err_msg = 'По технический причинам возникла ошибка при поиске'
                error_list.append(err_msg) if err_msg not in error_list else error_list

                if isinstance(err, GoogleHttpError):
                    return 'api_err'

        responses_df = self.clear_responses(responses_dict, req_count)

        return responses_df

    def clear_responses(self, responses_dict, req_count):
        result_df = pd.DataFrame.from_dict(responses_dict, orient='index')
        result_df = result_df.reset_index().rename(columns={'index': 'link'})
        result_df['rating'] = result_df['rating_list'].apply(lambda x: sum(x) / req_count)
        result_df = result_df.sort_values(by='rating', ascending=False).drop(columns=['rating_list'])
        result_df = result_df.loc[~result_df['domain'].isin(stop_sites_list)]
        result_df['rating'] = result_df.groupby('domain')['rating'].transform('sum')
        result_df = result_df.drop_duplicates(subset=['domain'], keep='first')

        result_df = result_df.loc[result_df['link'].apply(lambda x: self.clear_reestr_sites(x))]
        result_df = result_df.loc[result_df['link'].apply(lambda x: self.clear_documents_sites(x))]

        return result_df

    @staticmethod
    def count_requests(requests):
        """Статический метод для подсчета кол-ва поисковых запросов для конкретной
        огранизации и кол-ва запросов одного сообщения для всех организаций.

        Args:
            requests (dict): Вложенный словарь запросов, где ключ - нименования поисковика,
                заначение - словарь, где ключ - номер запроса организации, значение - список
                запросов по данной организации.

        Returns:
            tuple: Кортеж, сожержащий:

                **int**: Кол-во запросов для конкретной огранизации.

                **int**: Кол-во запросов в одном почтовом сообщении для всех организаций.
        """
        requests_count_by_query = {}
        requests_count = 0
        for search_engine, query in requests.items():
            for index_of_query, req_list in query.items():
                requests_count_by_query.update({index_of_query: len(req_list)})
                requests_count += len(req_list)
        return requests_count_by_query, requests_count

    @staticmethod
    def clear_reestr_sites(url):
        """Cтатический метод фильтрации адресов с цифрами в URL.

        Args:
            url (str): Строка адреса из поисковой выдачи в формате URL.

        Returns:
            int: Обновленный рейтинг адреса, в зависимости от
            содержимого строки URL.
        """
        url_tokens = re.split('/|-|_|=', url)
        any_token_contains_only_digit = False
        for token in url_tokens:
            if token.isdigit():
                any_token_contains_only_digit = True
                break
        if any_token_contains_only_digit:
            return False
        else:
            return True

    @staticmethod
    def clear_documents_sites(url):
        """Cтатический метод фильтрации адресов на документы (pdf, html и т.д.).

        Args:
            url (str): Строка адреса из поисковой выдачи в формате URL.

        Returns:
            int: Обновленный рейтинг адреса, в зависимости от
            содержимого строки URL.
        """
        url_ext = url.split('/')[-1].split('.')[-1]
        if url_ext in stop_docs_exts:
            return False
        else:
            return True

    @staticmethod
    def increase_rating_by_length(dataframe_with_ratings):
        """Метод для повышения рейтинга адресов с лаконичным URL.
        Величина повышения основана на самом длинном адресе в выборке и находится в интервале [0, 1)"""

        result_df = dataframe_with_ratings.copy()
        link_length_proportion = result_df['link'].str.len() / result_df['link'].str.len().max()
        result_df['rating'] = result_df['rating'] + (result_df['rating'] * (1 - link_length_proportion))
        result_df['rating'] = result_df['rating'].round(2)
        return result_df

    def grouped_by_query_df(self, search_engine_query_relevance_dict, query_params,
                            requests_count_by_query, user_logger):
        """Создание датафрейма на основе обработки всех результатов поисковой выдачи.

        Args:
            search_engine_query_relevance_dict (dict): Словарь, в котором содержится
                информация о рейтинге адресов для конкретного запроса организации при использовании
                конкретной поисковой машины. Ключ - поисковая машина, значение - словарь, где
                ключ - номер запроса организации, значение - адреса и список рейтингов, которые
                были им присвоены по запросам для данной организации.
            query_params (dict): Словарь содержимого вложенного файла - msg_details['att'].
            requests_count_by_query (dict): Кол-во запросов для конкретной огранизации.
            user_logger (Logger): Объект позволяющий логировать события по конкретному пользователю.

        Returns:
            dict: Результирующий словарь, в котором для каждого адреса по
            конкретным запросам сформирован средний рейтинг
            на основе списка рейтингов поисковой выдачи.
        """
        df = pd.DataFrame(columns=['Поисковик', 'Номер запроса', 'Адрес ресурса',
                                   'Заголовок', 'Рейтинг Адреса'])

        try:
            for search_engine, query_index in search_engine_query_relevance_dict.items():
                for index, links in query_index.items():
                    for i, (link, details) in enumerate(links.items()):
                        s = pd.Series(data=[search_engine, index, link,
                                            details['title'], details['rating']],
                                      index=df.columns)
                        df = df.append(s, ignore_index=True)

            # df['Рейтинг Адреса'] = df['Список Релеванстности'].apply(
            #     lambda x: sum(x) / (self.engine_results_number * requests_count_by_query[index]))

            query_list = df['Номер запроса'].unique()
            grouped_df = df.groupby('Номер запроса')

            user_logger.add_log('Успешно создан датафрейм с результирующими рейтингами адресов.')

        except Exception as err:
            user_logger.add_log(
                'Ошибка при создании датафрейма с рейтингами адресов: {}. Детали: {}.'.format(
                    repr(err), ''.join(traceback.format_tb(err.__traceback__))
                )
            )

        else:
            df_by_query_dict = {}
            for group_index in query_list:
                group_df = grouped_df \
                    .get_group(group_index) \
                    .sort_values(by='Рейтинг Адреса', ascending=False) \
                    .head(self.results_to_send_number)
                org_inn = query_params[group_index]['inn']
                org_name = query_params[group_index]['name']
                org_address = query_params[group_index]['address']
                org_description = query_params[group_index]['description']

                group_df = group_df.drop(columns=['Номер запроса'])

                # Группируем результаты в словари с ключами по поисковым машинам
                group_df_by_search_engine = dict(tuple(group_df.groupby('Поисковик')))
                for k, v in group_df_by_search_engine.items():
                    v.drop(columns=['Поисковик'], inplace=True)
                    group_df_by_search_engine[k] = v.head(3)

                df_by_query_dict[group_index] = {'inn': org_inn, 'name': org_name,
                                                 'address': org_address, 'description': org_description,
                                                 'search_engines': group_df_by_search_engine}

            return df_by_query_dict

    def send_result(self, msg, user_logger):
        """Отправка результата пользователю на почту с помошью объекта класса ``MailBoxer``.

        Args:
            msg (dict): Обновленный словарь ``msg_details``, в котором содержится
                вложенный файл, если обработка сообщения прошла успешно, а также
                содержатся сообщения об ошибках, которые возникле в ходе обработки.
            user_logger (Logger): Объект позволяющий логировать события по конкретному пользователю.
        """
        try:
            msg_to_send = self.mailboxer.create_msg(msg_content=msg)
            self.mailboxer.send_mail(msg=msg_to_send)
            user_logger.add_log('Сообщение c результатами успешно отправлено.')

        except Exception as err:
            user_logger.add_log(
                'Ошибка при отправке сообщения: {}. Детали: {}.'.format(
                    repr(err), ''.join(traceback.format_tb(err.__traceback__))
                )
            )

    @staticmethod
    def get_attachment(att, msg, user_logger):
        att_helper = InputAttHelper(att, errors_list=msg['errors'], user_logger=user_logger)
        query_rows = att_helper.parse_att()
        return query_rows

    def do_work(self):
        """Метод, который организует сценарий работы программы.
            При этом сообщения обрабатываются по очереди, сначала более ранние.
        """

        msgs = self.get_messages(move_to_archive=True) if not None else []
        msgs_count = len(msgs)
        if msgs:
            for msg_index, msg_content in enumerate(msgs):
                user_logger = self.logger.copy(from_=msg_content['from'],
                                               subj_=msg_content['subject'],
                                               received_time=msg_content['received_time'])
                user_logger.add_log('Началась обработка сообщения {} из {}.'.format(
                    msg_index + 1,
                    msgs_count
                )
                )

                att_content = self.get_attachment(att=msg_content['input_attachment'], msg=msg_content,
                                                  user_logger=user_logger)
                if att_content:
                    relevance_dict_by_query, requests_count_by_query = self.search_query(
                        query_params=att_content,
                        error_list=msg_content['errors'],
                        user_logger=user_logger
                    )

                    dict_of_df = self.grouped_by_query_df(
                        search_engine_query_relevance_dict=relevance_dict_by_query,
                        query_params=att_content,
                        requests_count_by_query=requests_count_by_query,
                        user_logger=user_logger,
                    )

                    output_att_helper = OutputAttHelper(dict_to_attach=dict_of_df, user_logger=user_logger)
                    msg_content['result_attachment'] = output_att_helper.create_att()
                self.send_result(msg=msg_content, user_logger=user_logger)
                user_logger.push_logs()
        else:
            self.logger.add_log('Входящих сообщений не найдено')
            # self.logger.push_logs(from_='system', subj_='')
